#!/bin/sh
set -eu
apk add hugo git nodejs npm
cd themes/winny2-theme
npm i
cd -
ln -s themes/winny2-theme/node_modules node_modules 
npm install -g postcss-cli autoprefixer

hugo
