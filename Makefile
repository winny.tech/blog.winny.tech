HUGO_ARGS=--logLevel info

THEME_DIR=themes/winny2-theme

dev: prepare
	hugo server --buildFuture --buildDrafts --disableFastRender --watch $(HUGO_ARGS)

prepare: node_modules
	$(MAKE) -C $(THEME_DIR) prepare

node_modules: $(THEME_DIR)/node_modules
	ln -sf $< $@

$(THEME_DIR)/node_modules: $(THEME_DIR)/package.json
	$(MAKE) -C $(THEME_DIR) node_modules

ci:
	apk add hugo git nodejs npm
	git config --global --add safe.directory $(PWD)
	$(MAKE) prepare
	npm i -g postcss-cli autoprefixer
	$(MAKE) public

# NB is PHONY because idk how to track dependencies.
public: prepare
	hugo $(HUGO_ARGS)


clean:
	rm -f node_modules
	rm -rf public
	$(MAKE) -C $(THEME_DIR) clean

.PHONY: dev prepare clean public ci
