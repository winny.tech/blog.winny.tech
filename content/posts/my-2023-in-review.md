+++
title = "My 2023 in review"
author = ["Winston (winny) Weinert"]
date = 2023-12-21T00:00:00-06:00
tags = ["lifestyle"]
draft = false
cover = "future-path.jpg"
+++

{{< figure src="/ox-hugo/future-path.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Dietmar Rabich CC BY-SA 4.0 ([link](https://commons.wikimedia.org/wiki/File:D%C3%BClmen,_Umland,_Sonnenaufgang_--_2012_--_8069.jpg))" >}}

Things that I achieved this year.

1.  Exercise.  I was going 3 times a week to the gym.  Now I'm working out at
    home about once a week.
2.  A no-processed-foods diet.
3.  Occasional respite from some health issues.  I'm not dieing so that's pretty cool.
4.  Remained self-employed.  Even after many project transitions including
    firing a toxic client.
5.  Worked with some new technologies including Love2d, Fennel, Angular 3,
    Next.js, NestJS.
6.  Completely exit social media.
7.  Quit IRC and shed the cloud of negative energy befallen this waning community.
8.  Started my hand at entrepreneurship and small businesses - currently
    experimenting with resales and tutoring.
9.  Realized that I don't need to work in tech if it doesn't bring me joy.  If I
    find a tech job that resonates with me, great, if not, that's okay too.
    Life is too short to put up with a negative employment experience.
10. Let go of a bunch of stuff that wasn't good for me anymore.  Some were
    technologies that I ingrained into my personality (woops, don't do this!),
    and others were spending less time around folks that were mostly negative energy.


## Goals for 2024 {#goals-for-2024}

1.  Even more exercise.  Working out at home (for me) requires more discipline and
    routine.
2.  Cover all my living expenses with my small businesses.
3.  Consider the next place to live.  Milwaukee doesn't offer the best weather.
    Nor does it offer many other benefits that I seek for in a city (such as a
    safe and secure experience when cycling or walking - no reckless autos).
4.  Figure out how to monetize my writing.
5.  Create (content?) every week in a way that is share-able with others.


## Thanks {#thanks}

Friends and family make you.  So this is a little note:  thanks to everyone for
being kind, patient, honest with each other in 2023.  Let's keep it up in 2024.

Keep it real.
