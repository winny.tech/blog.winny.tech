+++
title = "No need to reinstall your OS"
author = ["Winston (winny) Weinert"]
date = 2021-07-08T21:01:42-05:00
tags = ["computing", "linux"]
draft = false
+++

A fantastic "feature" of Linux, BSD, and even Windows 10 is you don't really
need to reinstall to migrate an installation to a new computer.  A common
misunderstanding is if you get a new PC, you must use the new OS install, or
install a new copy of your OS.  If you're intending on replacing an existing
PC (and disposing of or re-purposing the old one), there is probably no need to
reinstall your OS and deal with user data migration.

On Linux if you rip a SSD out of **any** x86 box, it will probably boot on any
other machine, including virtualization software.  One example of this is the
laptop I am currently typing away on.  It is the third laptop for the same
Linux installation.  Each time I simply either moved the SSD over or duplicated
the SSD onto another SSD.  Sometimes I have to modify some configuration, as I
use Gentoo on my workstation and laptop and it's not going to do everything for
me automagically (or otherwise surprise me with some unwelcome, opinionated
defaults).

My NAS has been migrated between multiple rebuilds with no problem.  Even
switching between SAS Passthrough cards is not a problem.  It runs
Alpine... but the distro hardly matters in this discussion.  All distros will
handle booting on random equipment reasonably well.  My Desktop has survived a
complete rebuild (new everything) and many partial rebuilds.  It's the same
install from years ago.

Sometimes on IRC I hear of people having the same install from ago.  I think it
might be more common in the Gentoo community, but I don't see this being a
distro specific thing, only that Gentoo is a bit less surprising when things
break.  Clear error messages and doesn't hide it behind a bunch of tooling that
takes a long time to learn.


## Example migration {#example-migration}

I recently swapped out my HP Elitebook 820 G4 for a XPS 13.  Both devices have
NVMe SSDs.  I saw four options to perform the installation migration:

1.  Install the old SSD into the new laptop;
2.  Remove the old SSD.  Install into a USB SSD enclosure.  Then copy its
    contents over (using `ddrescue`) to the new SSD (using a LiveCD environment);
3.  Boot the old laptop with a LiveCD, make an image of the SSD onto an external
    drive, then boot the new laptop with a LiveCD, write the image onto the new
    SSD from the external drive;
4.  Or Boot both laptops with a LiveCD, network them, then the old SSD data to
    the new SSD on the other machine using the network.

I opted for option 4.

First step is to boot both laptops with live media.  I prefer GRML because it's
pretty simple and no frills, yet it ships with normal desktop stuff such as
audio, if you really need it.  It has a boot option to load the entire live
media into ram, thereby allowing you to remove the bootable media.  (One could
also PXE boot to achieve similar effect.)  I simply booted each laptop from the
same usb, using the option to load into memory.  Next up was to set up a
network.  In this case I had 1Gb ethernet interfaces on both devices (the XPS
13 has ethernet via a USB-C dock).  I simply ran `grml-network` on both, said
no to DHCP, set each to a RFC 1918 address in the same configured subnet.  Ping
one host from the other and verify it works.  Next is a bit muddy, because it
in theory did not work perfectly, but it did achieve the intended results.

I ran `nc -l 1234 | pv > /dev/nvme0n1` on the new machine.  Then I ran `pv
/dev/nvme0n1 | nc 192.168.0.1 1234` on the donor machine.  It took about 2.5
hours for coping an entire 1TB SSD.  In retrospect I believe this could have
been faster had I used 10Gb ethernet or a USB enclosure, but it got the job
done.

One gotcha is when the imaging finished, nc/pv hung on both sides.  I could
tell they were finished because no more progress was being made.  And they were
at the progress of the size of the donor machine's storage.  This is not a good
thing to experience, so any recommendations to get better `nc ... | pv ...` to
exit is welcome 😁.

Next up was booting.  I just rebooted and it worked.  Note: I set up my grub
installs to boot both with MBR BIOS systems and UEFI systems.  So tell your
friends, you don't need to pick 'n choose how to boot your PC, just boot it
either way if you set up grub to support both.


## Migrating Windows Installs {#migrating-windows-installs}

I don't know if it is realistically feasible to migrate a pre-Windows 10
install, but with Windows 10, one can simply yank the OS disk and install it in
the new PC.  If you need to copy tho install onto a soldered SSD, or are
upgrading the storage, you can just image the entire original storage device
onto the destination storage device.  One can use gparted to resize Windows.
Surprisingly, Windows will handle this - previously Windows would get upset if
the NTFS `C:` partition is resized without special treatment.

Alternatively, there are vendor tools that can streamline this process, such as
[Samsung Data Migration tool](https://www.samsung.com/semiconductor/minisite/ssd/download/tools/).[^fn:1]

One gotcha - even if you successfully migrate your Windows 10 install to a new
box, you may have to reactivate the license... you'll find out.


## Some other gotchas {#some-other-gotchas}

If you need to resize an install, it might be feasible without much trouble.
If you're growing a Linux install, usually one can just resize the
partitions/block devices in gparted/parted, **then** run grow2fs(8) on the
filesystems.  If you're using LUKS encryption it appears to take its size from
the size of the block device it resides on.  AS for LVM2, see pvresize(8).

Shrinking is a bit trickier because you will have to adjust the
LVM2/LUKS/filesystems **before** shrinking the partition itself.  Shrinking can
be trickier because doing partition table math is hard.  You'll have to
carefully ensure you shrink the filesystem, then shrink the block devices, and
partitions in order.  I haven't quite figured it out but it feels like
guesswork to get the sizes just right, so it might be easiest to shrink the
filesystem, LVM2 physical volume, LUSK, etc to be extra small, but a bit bigger
than whatever they contain.  Then just "grow" each filesystem/block device
after the partition is finally shrank.  This way you don't need to do exact
math.  Recap: Shrink everything be too small starting with the filesystem
working your way out, then grow everything inside the partition with growing
the filesystem last.

Backups!  Backups!  Backups!  If you are doing anything with data, it doesn't
exist if you don't have backups that you can restore from.  Ideally you want at
least two copies.  When imaging anything, it's usually a good idea to keep a
copy of the original image until you're satisfied with your handywork.  Better
safe than sorry.  Data storage is cheap... or if you afford backups for your
storage, you can't afford your storage (as it doesn't exist without backups).


## Conclusion {#conclusion}

I think my details were a bit streamlined, but I wanted to keep this short.  If
the reader takes anything away from this post, most modern OS installs are
portable between hardware, and are easy to install onto new storage.
Reinstalls shouldn't be a thing of necessity, but an action of policy only - if
you are re-purposing a machine, by all means reinstall, but no need to do it
when migrating.

As always I should shout out a live media to use for this post --- [GRML linux](https://grml.org/)
is great for this.

[^fn:1]: Thanks smokey991 for suggesting this.