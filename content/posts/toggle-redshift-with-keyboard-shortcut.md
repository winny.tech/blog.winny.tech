+++
title = "Toggle Redshift with Keyboard Shortcut"
author = ["Winston (winny) Weinert"]
date = 2019-01-09T08:04:00-05:00
tags = ["linux", "computing"]
draft = false
+++

Redshift is a screen-tinting program that achieves similar goals to
the popular f.lux[^fn:1] program.

I perused through the redshift man-pages and noticed there is no
documented way to toggle redshift. Of course one can click the
notification area icon when using redshift-gtk or `SIGTERM` the
redshift process, but neither is very user friendly. (The mouse is not
user friendly.) After some awkward DuckDuckGo-ing and Googling I found
an obvious solution on the redshift homepage[^fn:2]: simply send `SIGUSR1` to
the redshift or redshift-gtk process. When using redshift-gtk, one can
choose to send `SIGUSR1` to either redshift or redshift-gtk.

This is the script I came up with:

```sh
#!/bin/sh
set -eu
if ! pkill -x -SIGUSR1 redshift; then
    echo 'Could not find redshift process to toggle.' >&2
    exit 1
fi
```

After installing the script into my system's `PATH`, now all I have to
do is add a line to my Xbindkeys[^fn:3] configuration file (`~/.xbindkeysrc.scm`) such as:

```scheme
(xbindkey '(Mod4 F2) "toggle-redshift")
```

Now I can type `Mod4-F2` and toggle Redshift.

[^fn:1]: [f.lux homepage](https://justgetflux.com/)
[^fn:2]: [Redshift homepage § Configuration Options](http://jonls.dk/redshift/#configuration-options)
[^fn:3]: [XBindKeys homepage](http://www.nongnu.org/xbindkeys/xbindkeys.html)