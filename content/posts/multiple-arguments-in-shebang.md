+++
title = "Multiple arguments in shebang"
author = ["Winston (winny) Weinert"]
date = 2024-01-10T00:00:00-06:00
tags = ["computing", "linux", "emacs", "perl"]
draft = false
cover = "shebang-cover.png"
+++

{{< figure src="/ox-hugo/shebang-cover.png" caption="<span class=\"figure-number\">Figure 1: </span>Jamian · CC BY 3.0 Deed ([link](https://commons.wikimedia.org/wiki/File:Monitorin_The_Street_(160498125).jpeg))" >}}

A frequent quip of the unix-beard is shebangs _cannot_ contain multiple command-line arguments.
Let's break it down and see where this assumption no longer holds true.


## What is a Shebang? {#what-is-a-shebang}

The shebang is the line at the beginning scripts such as Python and Shell
scripts that instructs the OS how to execute the script.  Looks something like
`#!/bin/sh` or `#!/usr/bin/env python`.  This line specifies how to _interpret_
the file.  This progam specified on that line is also known as an
_interpreter_.  Without the line, the OS wouldn't know what to do.  By default,
shebang-less scripts are executed with the same program as the parent process.
One might experience this if saving a Perl script without a shebang, then
executing it.  Your shell will likely try to run it as a shell script.

This is why a shebang is essential to most scripts.  Without one, there is no
guarentee it will run with the correct interpreter besides manipulating the
parent process to choose a specified interpreter.

Wikipedia has an [excellent page](https://en.wikipedia.org/wiki/Shebang_(Unix)) on the shebang.  The [`execve(2)` manpage](https://man7.org/linux/man-pages/man2/execve.2.html) (try
running `man 2 execve`) explains the shebang in further detail.


## An example... in Perl? {#an-example-dot-dot-dot-in-perl}

Consider this short Perl script.  It prints the first line and last line, or
just the first line, or no lines.  This script uses `perl -n` to wrap the body
of the program in `while(<>) { ... }` loop.  In short, `perl -n` reads each
line, then executes the body.  Since I need the `-n` argument on the command
line, I tried this first:

```perl
#!/usr/bin/env perl -n
# Why won't this work :-o
print if ++$lines == 1;
print if eof() && $lines > 1;
```

If I save it and run it, I get the following error output:

```text
$ /tmp/a.pl  < /etc/passwd
/usr/bin/env: ‘perl -n’: No such file or directory
/usr/bin/env: use -[v]S to pass options in shebang lines
```

GNU coreutils env documentation [offers insight](https://www.gnu.org/software/coreutils/manual/html_node/env-invocation.html#g_t_002dS_002f_002d_002dsplit_002dstring-usage-in-scripts):

> The `-S/--split-string` option enables use of multiple arguments on the first
> line of scripts (the shebang line, `‘#!’`). ... Most operating systems
> (e.g. GNU/Linux, BSDs) treat all text after the first space as a single
> argument. When using env in a script it is thus not possible to specify
> multiple arguments.

Alright, ok.  TIL about this `-S` flag, neat!  If I change the shebang line to
`#!/usr/bin/env -Sperl -n`, it works:

```text
$ /tmp/a.pl  < /etc/passwd
root:x:0:0:System administrator:/root:/run/current-system/sw/bin/bash
nobody:x:65534:65534:Unprivileged account (don't use!):/var/empty:/run/current-system/sw/bin/nologin
```

Viola!  Passing arguments in the shebang without any funny hacks!

If I add the suggested `-v` flag (`#!/usr/bin/env -vSperl -n`), `env` prints
helpful debug information before executing the script:

```text
split -S:  ‘perl -n’
 into:    ‘perl’
     &    ‘-n’
executing: perl
   arg[0]= ‘perl’
   arg[1]= ‘-n’
   arg[2]= ‘/tmp/a.pl’
```

It's all documented in the coreutils documentation available [online](https://www.gnu.org/software/coreutils/manual/html_node/env-invocation.html) or via
`info coreutils env` in your terminal.


## Where can I use this feature? {#where-can-i-use-this-feature}

Any modern OS that ships [`env` from coreutils](https://www.gnu.org/software/coreutils/manual/html_node/env-invocation.html) including the majority of popular
desktop/server Linux distros.  If you're a Mac user [you're also in luck](https://ss64.com/mac/env.html), thanks
to [FreeBSD's downstream `env` implementation](https://man.freebsd.org/cgi/man.cgi?env) supporting `-S` too.

[POSIX `env`](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/env.html) does not offer this `-S` feature.  [OpenBSD](https://man.openbsd.org/env.1), [NetBSD](https://man.netbsd.org/env.1), [Busybox](https://web.archive.org/web/20231216062005/https://busybox.net/downloads/BusyBox.html#eject), [Toybox](https://landley.net/toybox/help.html#env)
are also without `-S`.  Busybox ships on some Linux distros such as Alpine.  My
first step when using these hosts is to install software to reduce the friction
of productivity including coreutils.  Coreutils is my portability strategy for
`env -S`.


## Can't use `env -S` or need POSIX compliance? {#can-t-use-env-s-or-need-posix-compliance}

If one cannot use `env -S`, I believe the time-tested approach is to write a
wrapper program for each tool that needs specified arguments on the shebang
line.  With the above example, I could put this script in my `$PATH` and name
it `perlloop`:

```sh
#!/bin/sh
exec perl -n "$@"
```

Next, I can update the shebang from the above example to `#!/usr/bin/env
perlloop`.  Then it'll work the same


### Why not just use `#!/usr/bin/perlloop`? {#why-not-just-use-usr-bin-perlloop}

Works too if that's where `perlloop` lives.  You probably don't care where it
lives though.  Just want it to find the matching program in your `PATH`.  That
is why I prefer using `#!/usr/bin/env ...`.  See the [coreutils env
documentation](https://www.gnu.org/software/coreutils/manual/html_node/env-invocation.html#g_t_002dS_002f_002d_002dsplit_002dstring-usage-in-scripts) for additional discussion on the tradeoffs.


## Emacs and Shebangs {#emacs-and-shebangs}

My Emacs setup checks for a shebang, and ensures the file is executable on
save.  [Check it out here.](https://github.com/winny-/emacs.d/blob/master/configuration.org#make-shebanged-files-executable-on-save)  I suggest adding a hook to your text editor to ensure
your scripts are executable on save.  It saves a lot of headache.

I also [wrote an experimental feature](https://github.com/winny-/emacs.d/blob/master/site-lisp/shebang-change.el) to auto-matically changes the syntax
highlighting and major mode when the shebang changes in the Emacs buffer.  The
result is not fiddling about with distracting stuff like re-opening the file or
using `M-: (auto-mode) RET`.

Shebangs are pretty cool.
