+++
title = "Set up a Private GitLab Runner on Alpine Linux"
author = ["Winston (winny) Weinert"]
date = 2022-01-29T23:00:21-06:00
tags = ["infrastructure", "linux", "alpine", "gitlab"]
draft = false
cover = "gitlab-runner-ansible.png"
+++

GitLab has recently [locked down the accessibility to free CI/CD minutes](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/).  You
now need to provide a Credit Card to prove you're a human.  Apparently
cryptofriends were using the CI/CD minutes to mine for cryptocurrencies.
Huh... if I had lesser ethics I'd probably do the same thing!  Kind of
brilliant to be honest.  Anyway, the end result is if you want users to
contribute to your project they need to either provide a CC or better yet, you
can set up private GitLab runners.  Just make sure to disable usage of shared
runners, or they'll continue to get the CC nag, and their builds will autofail.

Another reason to use your own GitLab runners is they'll probably be faster
than the shared runners.  Additionally if you have private GitLab projects,
your CI/CD minutes are numbered.


## BTW - Here's an Ansible playbook {#btw-here-s-an-ansible-playbook}

{{< figure src="/ox-hugo/gitlab-runner-ansible.png" caption="<span class=\"figure-number\">Figure 1: </span>screenshot demonstrating success using the playbook" >}}

I wrote a short playbook based off of the steps outlined below.  You can check
it out here: <https://gitlab.com/winny/gitlab-runner-playbook/-/tree/master>


## Set up the community repository {#set-up-the-community-repository}

Edit `/etc/apk/repositories`.  Uncomment the line that looks like
`http://dl-cdn.alpinelinux.org/alpine/v3.15/community`.  This is needed for
installing the needed software.


## Install docker {#install-docker}

First you have to install docker and start it up:

```sh
apk add docker  # Install docker
rc-service docker start  # Start the service
rc-update add docker  # Start the service on subsequent boots
```


## Install GitLab runner {#install-gitlab-runner}

Next let's install the gitlab-runner program.  `shadow` which contains
`gpasswd` which is a simple way to add a user to a group.

```sh
apk add gitlab-runner  # Install gitlab-runner
apk add shadow  # Needed for 'gpasswd'.
```

Next add the `gitlab-runner` user to the docker group.

```sh
gpasswd -a gitlab-runner docker
```

Register the GitLab Runner.  You'll need to specify a default docker image.  I
usually do `alpine:latest`, but it shouldn't matter because the `image` should
be specified in your `.gitlab-ci.yml` for maximum clarity.  You will also need
the registration token from your GitLab project's CI/CD page.  You can find it
by clicking on Settings -&gt; CI/CD -&gt; expand "Runners".  This is also where your
runners will be listed.

```sh
gitlab-runner \
     --non-interactive \
     --run-untagged \
     --url https://gitlab.com/ \
     --executor docker \
     --name 'name to identify your runner in the gitlab UI' \
     --docker-image alpine:latest \
     --registration-token your-registration-token-here
```

Now fix the permissions.  It appears this file is not readable by the
gitlab-runner user (though alpine does set this user up to utilize this file -
weird!).  Let's change it so it belongs to the gitlab-runner group and is
readable by that group:

```sh
chgrp gitlab-runner /etc/gitlab-runner
chmod 0750 /etc/gitlab-runner
chgrp gitlab-runner /etc/gitlab-runner/config.toml
chmod 0640 /etc/gitlab-runner/config.toml
```

Optional, add syslog logging.  Add `--syslog` to your
`/etc/conf.d/gitlab-runner`.  The file should look like the following:

```sh
# Extra options passed to 'gitlab-runner run'
GITLAB_RUNNER_OPTS="--config /etc/gitlab-runner/config.toml --working-directory /var/lib/gitlab-runner --service gitlab-runner --syslog"

# Change to root if you want to run a system instance instead of a user one
GITLAB_RUNNER_USER="gitlab-runner"
# Same as above
GITLAB_RUNNER_GROUP="gitlab-runner"
```

Next, let's start and enable the runner

```sh
rc-service gitlab-runner start
rc-update add gitlab-runner
```


## Now test if it works! {#now-test-if-it-works}

1.  Test if the runner is visible on the page where the registration token can
    be found (Runner settings).

2.  Trigger a gitlab CI job with the new runner.


## Troubleshooting {#troubleshooting}

Edit `/etc/init.d/gitlab-runner`.  Add `--debug --syslog` between the "run"
subcommand and the `gitlab-runner` program.  So the relevant line should look
like this:

```sh
command_args="--debug run --syslog ${GITLAB_RUNNER_OPTS}"
```

Now you can check `/var/log/messages` for errors.


## Final remarks {#final-remarks}

It's a good idea to use a VM server to deploy a gitlab runner that can use
docker.  This way you can contain a lot of the security concerns to a single VM
guest.  You also do not need to expose your guest to the internet or even the
local LAN - it only needs to dial out via a NAT.

Another cool thing learned when working on this is one can use the
[`community.libvirt.libvirt_qemu`  connection plugin](https://docs.ansible.com/ansible/latest/collections/community/libvirt/libvirt_qemu_connection.html) to manage guests without even
needing SSH access or credentials.  This uses the [qemu guest agent](https://wiki.libvirt.org/page/Qemu_guest_agent), bypassing the
need for suitable networking to manage guests.

Stay tuned for more infrastructure posts.
