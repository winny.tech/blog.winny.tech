+++
title = "Racket frustrates me"
author = ["Winston (winny) Weinert"]
date = 2023-06-16T03:30:00-05:00
tags = ["racket", "rant"]
draft = false
cover = "racket-wat.png"
+++

## 2024-07-06 I've archived this post {#2024-07-06-i-ve-archived-this-post}

Looking for the post _Racket frustrates me_?  You can read the [original post](https://web.archive.org/web/20240110183908/https://blog.winny.tech/posts/racket-frustrates-me/) on
The Internet Archive's Wayback Machine.


### Why archive this post? {#why-archive-this-post}

It's been about a year since I published this post and since realized
something: I don't have any skin in the game anymore.  My workflow is now 100%
Racket free.  It no longer matters to me if Racket becomes a whopping success
or a sullen failure.  _Racket exists.  It just is._ Due to my divestment from
the Racket ecosystem, I haven't kept current with the growth of the ecosystem.
Consequently, my concerns about Racket are probably out of date.

Racket was an exciting exploration into the one possible future of computing.
I became emotionally invested in its potential and felt let down as my
idealization of what Racket _could be_ drifted further from reality of what
Racket _is_.  There was greatness in store for Racket, alas it wasn't to be.
