+++
title = "About my keyboard choices"
author = ["Winston (winny) Weinert"]
date = 2020-11-03T21:37:00-05:00
tags = ["computing", "ergonomics"]
draft = false
cover = "kb600-angled-l-2007x1024-1400x1049.jpg"
+++

> Disclaimer: Dvorak, fancy keyboards, whatever, does not replace good lifestyle
> habits such as computer breaks, good desk ergonomics, and balancing one's
> computer life with **gasp** real life.  Dvorak and fancy keyboards can make your
> life better, but they cannot completely address RSI problems alone.

Since sometime 2013 I have faced pain, numbness, and tightness in my hands due
to computer overuse.  At first I chose to ignore it, but it got so bad I'd take
days if not weeks off from prolonged computer use.  At my worst, I saw
healthcare professionals which treated me for repetitive strain injury (RSI).
It took six months to a year to manage my symptoms well enough to start
participating in open source again.  Shortly after, I landed a long-term
freelance gig as a system administrator.  The increased computer work led to a
resurgence of RSI symptoms.  I managed for awhile with simply taking breaks,
maintaining okay desk ergonomics, doing tendon &amp; nerve glides, and hot/cold
therapy.  At some point my symptoms were not improving, so I looked into using
better input devices.


## Strategic switch to Dvorak {#strategic-switch-to-dvorak}

{{< figure src="/ox-hugo/900px-KB_United_States.svg.png" caption="<span class=\"figure-number\">Figure 1: </span>The standard QWERTY layout, [courtesy Wikipedia](https://commons.wikimedia.org/wiki/File:KB_United_States.svg)." >}}

{{< figure src="/ox-hugo/900px-KB_United_States_Dvorak.svg.png" caption="<span class=\"figure-number\">Figure 2: </span>The standard Dvorak layout, [courtesy Wikipedia](https://commons.wikimedia.org/wiki/File:KB_United_States_Dvorak.svg)." >}}

Enter Dvorak.  I chose to switch to the [Dvorak keyboard layout](https://en.wikipedia.org/wiki/Dvorak_keyboard_layout) because it
requires less stretching to type the same English text.  Dvorak also is
designed such that each other key is likely typed by the other hand.  As such
each hand gets opportunity to rest between keystrokes, and position itself
where it needs to be for the next key.  The most frequent
letters are on the home row right below one's fingertips.  This includes all
vowels: A, E, I, O, and U; and common consonants: T, H, N, S, D.  To type any
of these common keys, I don't need to move a single finger to another
location.  I simply press the key.  In reflection, this is a good move.  While
I don't believe this made me a faster typist, it certainly has made typing a
lot more comfortable due to less effort to type the same text.  Dvorak helped,
but, there's more.

Shortly after switching to Dvorak in 2013-2014 --- cold turkey, I should add
--- I decided to bite the bullet, and acquire a more ergonomic keyboard.  I
shopped around, I did try "split" budget ergonomic keyboards such as the
Microsoft contour and it felt the same.  It actually felt worse, honestly.
Something that did help me for awhile was using low impact keyboards, such as
scissor switch laptop keyboard.  I also tried out a couple mechanical
keyboards.  Nothing really seemed very comfortable.


## Kinesis Advantage: The best board {#kinesis-advantage-the-best-board}

{{< figure src="/ox-hugo/kb600-angled-l-2007x1024-1400x1049.jpg" caption="<span class=\"figure-number\">Figure 3: </span>Kinesis Advantage 2 top view, [Kinesis Corp](https://kinesis-ergo.com/shop/advantage2/)." >}}

{{< figure src="/ox-hugo/kb600-ls3a-1400x1049.jpg" caption="<span class=\"figure-number\">Figure 4: </span>Kinesis Adavantage 2 demonstration, [Kinesis Corp](https://kinesis-ergo.com/shop/advantage2/)." >}}

Enter the [Kinesis Advantage](https://kinesis-ergo.com/shop/advantage2/).  It felt outside of my budget, with a price tag
ranging of around 275-350 USD.  I immediately recognized the potential this
keyboard had, given the keys offloaded to the thumbs, the concave "key wells"
designed to contour my hands, and the built-in Dvorak keyboard layout, for when
I couldn't use the operating system's Dvorak setting.  Additionally it was a
true split keyboard, so my forearms could be oriented more perpendicular to my
body, instead of bent inwards towards the keyboard's center (In other words my
arms can rest at my side, instead of being awkwardly placed in front of my
chest).  Additionally this layout allowed for less twisting of the forearms,
thereby improving circulation and reducing keyboard-strain.

Some things became immediately apparently when I switched to the Kinesis
Advantage.  It would not be pain free.  Using it for a couple months led to
new soreness, but never any sharp or dull pains related to RSI.  The manual
mentions that using new keyboards like this exercise slightly different muscle
groups, therefore your body will take some time to adjust and build up
strength, even if it is ergonomically easier and more comfortable.  Another
thing that became apparently was I did not learn how to type properly.  When
typing on a normal keyboard, I would roll my wrists a frequently to use my
stronger fingers (and minimizing use of my pinky, which felt intuitive to use
as a new computer user).  I would also frequently look for keys despite touch
typing for a majority of keys.  Simply put, I would type keys with the wrong
fingers, sometimes with the wrong hand completely, leading to a harder time
getting used to a split keyboard like the Kinesis Advantage.

I persisted though.  And Here I am 6+ years later still using the Kinesis
Advantage.  I believe this keyboard has played a very important role in my
studies, professional development, and mental health.  It has provided me with
a mechanism to minimize RSI due to less-than-comfortable stretches and frequent
movements on normal keyboards.  It has taught me how to type properly.  It has
mitigated frequent pain, thereby improving my mood and helping me deal with
my own mental well being.

A significant benefit of the Kinesis Advantage keyboard is the built in
piezoelectric speaker that actuates when the a key circuit is closed.  Instead of
relying on the click of the key switches, I rely on the clicking sound produced
by this speaker.  It helps me reduce the amount of force I use when typing,
though I have always been a heavy typist, so an onlooker might think I am still
very heavy with my keyboard, but alas it is much better.  Additionally this
speaker helps me maintain pace.  Instead of not being sure if I actually typed
a key, the speaker unambiguously confirms this keystroke for me.  Since it
resides in the keyboard, and is handled by a low latency micro controller, it is
near-instantaneous in relation to the key switch actuation.  If the computer
were to handle this, there would be a noticeable latency.  It turns out desktop
computers are really poor at low latency sound without a lot of effort.  It's
just not worth the effort, let micro controllers do what micro controllers do
best!


## What about keyboard shortcuts? {#what-about-keyboard-shortcuts}

I use vi keys with no adjustment.  On Dvorak `jk` are on the bottom row of the
left middle &amp; ring fingers.  `hl` are on the right hand just to the left of
home row, and on the upper row of the pinky.  This might seem awkward, but if
you're jk hl and it's causing you RSI problems, regardless where it's located,
you probably should not be using a keyboard.  Sure it might be easier to reach
if it's still on home row, but you shouldn't feel strain typing **any** key on
your keyboard.  If you do, you must address that problem, through switching
keyboard, not using a keyboard, and most importantly changing keyboard habits
(e.g. relearn how to type properly).  The same applies to Emacs keys, though I
think it's less awkward in Dvorak, because p is not on the pinky, but that's a
minor concern as mentioned above, as one should be able to type all keys fine.

The other concern with keyboard shortcuts is how does one type
Control-c/Control-x/Control-v... or really any keyboard shortcut one is used
to.  First of all you're probably typing it with **one hand** which is a terrible
sin unto your hands.  Normal typing is very low impact on your hands because in
theory **you type a single key on each hand at most**.  This means If you type
"X", you will hold down shift with the opposite hand.  That is why there are
two shift keys.  This also means you don't have to apply force on two ends of
your hand to achieve a keyboard combination.  In light of this, I strongly urge
you, dear reader, to start "balancing" your keyboard shortcuts over two hands.
If you need to type Control-c, hold down that control with the opposite hand.
Sure it might seem slower at first, but if you're typing it frequently, it will
cut down on strain.  This is also why people "swear by vim".  It's because
vim's modal user interface does not require many multi-key keystrokes.  This is
also why some emacs users seem to get RSI, is they don't discipline their
modifier keys, and wonder why if they mash their entire hand onto the keyboard,
it starts to manifest pain in the relation to the needless micro-stresses the
inexperienced emacs user puts on it.

Finally, maybe you thought I would answer the question --- "But I know
keystrokes by their location on the keyboard".  I believe with a little bit of
critical thinking, you may deduce this is patently false.  Sure you may have
forgotten how you learned the keystrokes initially, but I am willing to bet you
initially thought every time (okay, I need to press control and x ---
Control-x).  If you develop muscle memory for your keyboard, using application
shortcuts follow easily, and are very malleable in my experience.  There may be
some amount of relearning involved, but it's good for oneself, because it
enables the user time to really recognize the keyboard shortcut, and properly
memorize it as a keyboard combination, instead of a gesture of the hand that
won't map well between keyboards that are in the different in the slightest way
--- shape, layout, and size.


## Final thoughts {#final-thoughts}

There is more to my RSI story.  Even though I use a Kinesis Advantage keyboard
most of the time, my endurance on a normal keyboard has improved significantly
due to properly re-learning how to type.  Additionally learning Dvorak also
helps with endurance on any keyboard.

And no... there isn't a problem using a weird keyboard layout.  If you are
doing any serious typing, simply switch the layout, or bring a keyboard that
can switch it in hardware, or bring your own computer.  It's that simple.  Life
is too short to care about using [QWERTY](https://en.wikipedia.org/wiki/QWERTY)... it was never designed with
ergonomics in mind.  Just think about the word "ergonomics".  On Dvorak, each
other key in "ergonomics" is on the other hand, so each hand has ample time to
switch position (if necessary) between keystrokes.  Take QWERTY on the other
hand.  One types it using only four digits across two hands, and each hand is
responsible for typing a string of 3+ characters.  Think about some common
words, plot out how you would type them in QWERTY, then plot them out in
basically **any** other keyboard layout.  You'll see how god awful QWERTY is for
your hands if you want to minimize weird stretching and reaches.

When I moved to Milwaukee for university, I met several people who were also
Dvorak users.  It was amazing to pair-program with folks who not only know
Dvorak, but are comfortable in vim and Emacs.  I even lived with a Dvorak
user.  We could share each others' computers without dealing with trivial
things like keyboard layouts.  Nowadays I don't think anybody knows I type
Dvorak until they wonder why I type slow at their desk.  I'm not self conscious
about it --- because I'm a practiced typist, hunting &amp; pecking for me looks
like a mildly slow touch typist using a new keyboard.  You might wonder why I
don't try to touch type qwerty, and no, I don't want to confuse myself.
Learning a new keyboard layout takes dedication, and I can only truly touch
type in Dvorak.

Thank you for reading my thoughts on using Dvorak and the Kinesis Advantage
keyboard.  I can talk about computer ergonomics all day, due to their
importance in my lifestyle.  Drop me a line, I'd love to hear from you.
Interested in learning Dvorak?  Stop.  Give it a deep think.  Maybe you don't
need to learn a new keyboard layout.  If you're not discouraged, load up
[keybr.com](https://www.keybr.com/) and practice.  Go cold turkey, it worked for me, and can work for
you.
