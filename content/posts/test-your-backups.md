+++
title = "Test your backups"
author = ["Winston (winny) Weinert"]
date = 2024-01-27T00:00:00-06:00
tags = ["backups", "computing", "nixos", "linux"]
draft = false
cover = "images/Backup_Backup_Backup_-_And_Test_Restores.jpg"
+++

{{< figure src="/ox-hugo/Backup_Backup_Backup_-_And_Test_Restores.jpg" caption="<span class=\"figure-number\">Figure 1: </span>[John from USA - CC-BY-2.0](https://commons.wikimedia.org/wiki/File:Backup_Backup_Backup_-_And_Test_Restores.jpg)" >}}

Watch out, things break, stuff catches fire.  Let's talk about backups.

> [Last post]({{< relref "another-nixos-23-11-upgrade-gotcha" >}}), I stated that I'm going to switch focus away from NixOS commentary.
> This is still the plan.  Today, I am still committed to NixOS thanks to
> technical debt created - migrations aren't for free.  Until then, enjoy my
> NixOS posting :).

Last fall, I wanted to reformat my laptop's NixOS deployment from BTRFS (encased
within LVM2 itself encased in LUKS) to a ZFS partition plus another swap
partition.  My Nix install is comprised of a few artifacts:

1.  My [git repository](https://gitlab.com/winny/nixos-configs) with the `flake.nix` and `flake.lock` files
2.  The workstation's `/secrets` folder, sensitive data for service accounts.
3.  The workstation's `/home` folder

Both `/secrets` and `/home` are backed up via [borgmatic](https://torsion.org/borgmatic/) (using [borg](https://www.borgbackup.org/)) on a
nightly basis via [a crufty old nixos module](https://gitlab.com/winny/nixos-configs/-/blob/master/common/borgmatic.nix) that I wrote ([example of usage](https://gitlab.com/winny/nixos-configs/-/blob/master/hosts/stargate/default.nix?ref_type=heads#L37)).
Both folders were also snapshotted by [BTRBK](https://github.com/digint/btrbk) every 15 minutes (via this nixos
configuration).  This frequent snapshotting policy will continue on the ZFS
reinstall powered by zfs-autosnapshot.

The first test was to verify the integrity of the backed up artifacts.  I was
able to execute a full restore from backup from within a virtual machine.  This
included adapting my laptop's flake configuration to the VM, rebuilding, then
executing the `borg extract` commands.

> Fun fact: `borg mount` and `rsync` is several times slower than running `borg
> extract` (using [BorgBase](https://www.borgbase.com/)).  Keep that in mind when executing restores - if you
> need a full restore or a restore of a subdirectory, consider `borg extract`.
> If you need to pick and choose many files, consider `borg mount`.

After the successful test restore, it was time to execute a final backup.  On
my setup that's as simple as `systemctl start backup`.  Then
boot a NixOS installer.  Invoking `parted /dev/nvme0n1`, I came up with the
following partition layout:

```text
Model: INTEL SSDPEKNU010TZ (nvme)
Disk /dev/nvme0n1: 1024GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:

Number  Start   End     Size    File system  Name  Flags
 1      1049kB  1000MB  999MB   fat32        boot  boot, esp
 2      1000MB  1007GB  1006GB               zfs
 3      1007GB  1023GB  16.2GB               swap
```

The swap partition is used in conjunction with NixOS's
`swapDevices.*.randomEncryption.enable` setting.  This swap partition is
encrypted using LUKS.  This encrypted device mapper device is used for
swap.

Then I followed my standard install instructions [here on this blog]({{< relref "nixos_migration" >}}).


## Recovery strategy {#recovery-strategy}

As part of this restore procedure, I tested my restore strategy.  It turned out
the thumb drive and QR code with the full disk encryption (FDE) key for the
thumb drive were compromised.  They simply didn't work - the QR code was of a
different key.

Had I needed to recover my setup in a data loss scenario, it is likely I would
had lost data due to not having access to recovery material.  I was at risk of
data loss.  Ooop!

{{< figure src="/ox-hugo/Paris_Tuileries_Garden_Facepalm_statue.jpg" caption="<span class=\"figure-number\">Figure 2: </span>[Alex E. Proimos - CC-BY-2.0](https://commons.wikimedia.org/wiki/File:Paris_Tuileries_Garden_Facepalm_statue.jpg)" >}}

Next I created new recovery material.  It consisted of two components: A
passphrase and an encrypted thumb drive.  They live together; the passphrase is
more of a "are you sure you want to open this?"  than a security measure.  The
encrypted thumb drive contains my PGP private keys (encrypted with each key's
_own_ passphrase) and password database encrypted against my "private use only"
key.

In order to restore from this media, first open the LUKS container via
`cryptsetup luksOpen /dev/disk/by-id/usb-...-part0 usbCrypt` then mount it via
`mount /dev/mapper/usbCrypt /mnt/usb`.  I can load the GPG keys into my gpg
then run `gpg --decrypt --output -
/mnt/usb/password-store/backups/stargate/passphrase` to get the backup borg
storage passphrase.  I can then set up borg to access my backup via accessing
my backup service's dashboard.

Finally I was able to run `borg extract ...` to kick off the restore on the
laptop.

From start to finish the redeploy and restore took 3 hours for the data restore
and another hour due to various tasks of how this procedure works.  It's not
super automatic, but hey, it's tested and it works!


## Well I guess the restore worked! {#well-i-guess-the-restore-worked}

{{< figure src="/ox-hugo/computer-rage.png" caption="<span class=\"figure-number\">Figure 3: </span>Data loss is possible with a sledge hammer and no backups" >}}

Test your backups.  Until you do, they are but a speculative investment; you're
not sure if they work.  In theory they say they should, however, who really
knows.  Nobody really knows.  ****Go test your backups.****  Haven't done it yet,
well, then, buy [this sledge hammer](https://www.amazon.com/Estwing-Strike-Drilling-Crack-Hammer/dp/B00433SC4Q/) and apply it to your storage devices, because
your data is worthless without tested backups - it could disappear at any time.
Theft, fire, [PEBKAC](https://en.wikipedia.org/wiki/User_error), Software Bug (like [Steam's infamous rm -rf script
bug](https://github.com/valvesoftware/steam-for-linux/issues/3671))... anything is possible.


### Want a T-shirt? {#want-a-t-shirt}

![](/ox-hugo/computers-were-a-mistake-front.jpg)
![](/ox-hugo/computers-were-a-mistake-back.jpg)

[I'm selling T-shirts](https://winny-10.creator-spring.com/listing/computers-were-a-mistake?product=369) with that sledge hammer fellow on the back and "COMPUTERS
WERE A MISTAKE" on the front.  Of course it's supposed to be cheeky and not too
serious - we must laugh at technology before it destroys our human identity.
And embrace the good parts.  Computers are fun, if you let 'em.  Have fun with
'em, but don't let 'em control every aspect of your being.
