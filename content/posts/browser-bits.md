+++
title = "Browser Bits"
author = ["Winston (winny) Weinert"]
date = 2025-01-07T00:00:00-06:00
draft = false
cover = "browser-bits.png"
+++

{{< figure src="/ox-hugo/browser-bits.png" >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/ieEmWW8hUKQ?si=630Bi91iaeIzJ0z-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

All the browser userscripts that I've been using were scattered across a
multitude of pastebin links and git repositories.  It became a bit of effort to
track them down to re-install in [Greasemonkey](https://www.greasespot.net/) (Firefox) or [Tampermonkey](https://www.tampermonkey.net/)
(Chrome) with the time expended searching exceeding the time saved using my
extensions.  No beuno.

Enter [Browser Bits](https://gitlab.com/winny/browser-bits/-/tree/main): a curation of userscripts.  (See [awesome-userscripts](https://github.com/awesome-scripts/awesome-userscripts) to
learn more!)  Ultimately, the repository will contain [Stylus](https://add0n.com/stylus.html) stylesheet
overrides and related hodgepodge.


## My top two userscripts {#my-top-two-userscripts}

Have a taste of my simplistic userscript utilization.  It keeps me from
breaking the computer, ... erm, website.


### MediaWiki Sidebar Toggle {#mediawiki-sidebar-toggle}

{{< figure src="/ox-hugo/mediawiki-sidebar-toggle.gif" >}}

I use my [mediawiki](https://www.mediawiki.org/wiki/MediaWiki) sidebar toggle userscript almost daily ([click to install](https://gitlab.com/winny/browser-bits/-/raw/main/userscripts/mediawiki-sidebar-toggle.user.js)).
[NethackWiki](https://nethackwiki.com/wiki/Main_Page), [Wikipedia](https://en.wikipedia.org/wiki/Main_Page), and more operate on the mediawiki stack.  The classic
Vector theme employs an obstinate sidebar devoid of a toggle button.  This
script adds a toggle back in and binds it to `Control-'`.


### Advent of Code Adjustments {#advent-of-code-adjustments}

{{< figure src="/ox-hugo/aoc-composite.png" >}}

[adventofcode.com](https://adventofcode.com/) has rather distracting styling ([click to install](https://gitlab.com/winny/browser-bits/-/raw/main/userscripts/adventofcode.user.js)).  From text
"glow" in lieu of typographically-sane emboldened text to a font choice which
fails to be legible with a light foreground and dark background, it can use
improvement.  This userscript does not specify a replacement font, no.  That's
for the user to decide!  Merely specify your default fonts correctly and the
userscript will _just work_.


## You can customize websites too {#you-can-customize-websites-too}

Install [Tampermonkey](https://www.tampermonkey.net/) or [Greasemonkey](https://www.greasespot.net/) then copy-paste a template into a new
userscript.  Give it a shot.  If you can pick up a modicum of the [browser
Javascript APIs](https://developer.mozilla.org/en-US/docs/Learn_web_development/Core/Scripting), you can inject stylesheets, remove unwanted elements, and
more.

As a bonus, if you post the script to a public URL ending in `.user.js`
visitors well be greeted with a prompt asking to install the userscript.  This
couldn't be easier!  Or you could be like me, dump the scripts into a Git
repository.

{{< figure src="/ox-hugo/greasemonkey-install-userscript.png" >}}

Keep it simple.  The web sucks a lot more than it ever did---vis a vis, the web
from the first half of my life---however all hope is not lost.  You too can fix
up the web enough to keep your technological sanity in check.


## PS 2024 in Review in the works {#ps-2024-in-review-in-the-works}

I've been reading through my journals, all 70,000 words from 2024.  It's a
slog.  Stay tuned for a _2024 in Review_ post once my
reading-while-navel-gazing-project arrives at completion.
