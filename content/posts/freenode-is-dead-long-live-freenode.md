+++
title = "Freenode is dead, long live Freenode!"
author = ["Winston (winny) Weinert"]
date = 2021-05-26T23:13:05-05:00
tags = ["community"]
draft = false
cover = "purged-freenode-channel-qutebrowser.png"
+++

> Note: this is my OWN opinion and not representative of any community entity.
> This is a summary of what I've experienced since the Freenode takeover.

(The new [Freenode](https://freenode.net/) is [Libera.chat](https://libera.chat/).)

{{< figure src="/ox-hugo/purged-freenode-channel-qutebrowser.png" link="/ox-hugo/purged-freenode-channel-qutebrowser.png" >}}


## Drama? {#drama}

I prefer to not take sides in online drama, but I feel like I have to err on
the side of not-nuking-and-paving IRC channels that have existed for decades.

Here's the summary of what's happened, from my perspective: Guy with a bunch of
money takes over a community operated network.  There was much controversy.  I
thought it wasn't a big deal because these things happen, but now we are at an
impasse.  The Freenode staff is operating a bot that searches for channels with
a topic that mentions the libera IRC network, then forcefully moves the
offending top-level channels (that is with a single `#`) to unofficial channels
(that is, with a `##` prefix).  This has the unfortunate effect of erasing the
community each of those channels has built up.

Most of the popular channels that still exist are increasingly inactive, with
\#bash, #git, #python all hardly active these days (a couple messages an hour).

I've heard of a serious moderation mess in the Freenode support channels, with
moderation being temporarily suspended because nobody wanted to censor the
negative feedback towards Andrew's new directions, but I try to stay far away
from that, that's not my fight.


## What channels have been erased? {#what-channels-have-been-erased}

> Disclaimer: I am assuming these are the actions of the present Freenode staff,
> and not a "rogue faction" caused by Andrew's reckless takeover and splintering
> of the community.  I think this is a reasonable assumption, but it is a pretty
> strong assumption nonetheless.

Any channel with mention of Libera.chat in its topic (e.g. "Hey everyone, we
are transitioning to libera.chat!") is automatically taken over by the Freenode
staff.  The channel is then redirected to a newly created unofficial (`##`
prefix) channel with the same name.  This has the unfortunate effect of erasing
the good will Freenode has built up over the decades with open source
communities and developers.  It also disrupts the community in each of these
channels, in that bots, users who do not autojoin have to be manually
reconfigured to move over to the replacement channel.  Being kicked and banned
automatically for participating is kind of lame, to say the least.  Almost all
the channels that were taken over this way have virtually zero presence in
their unofficial replacement channel counterparts.

Here is a short list of channels purged this way:

-   \#tnnt
-   \#NetHack
-   \#weechat
-   \#haskell
-   \#vim
-   \#emacs
-   \#sway
-   \#k-9
-   \#curl
-   \#zig
-   \#musl
-   \#archlinux
-   \#scheme
-   \#irssi
-   \#znc
-   \#qutebrowser
-   Wikipedia channels (e.g. #wikipedia)
-   \#curl


## Voluntary Purge {#voluntary-purge}

Some other channels have decided to make the decision on the behalf of their
users, to give an official message to their users where they have moved to.
This has the advantage of users will know where to find the new community
channels, unlike the above victims:

-   \#gentoo channels (moved to Libera.chat)
-   \#perl (moved to Libera.chat)
-   \#nixos (moved to Matrix)


## Still exist, but are transitioning {#still-exist-but-are-transitioning}

-   \#lisp (They're moving to "l i b e r a" --- this circumvented their purge-bot's
    detection)
-   \#alpine channels (moved to oftc, hence they were not purged for mentioning
    libera.chat)


## Some social media coverage {#some-social-media-coverage}

-   <https://twitter.com/andy_kelley/status/1397388932367601665>
-   <https://twitter.com/kmett/status/1397387923993944064>
-   [Freenode ops take control of 700 channels](https://news.ycombinator.com/item?id=27286628) ([direct link to post](https://mastodon.sdf.org/@kline/106299403921451814))


## General links about this controversy {#general-links-about-this-controversy}

-   [freenode now belongs to Andrew Lee, and I'm leaving for a new network.](http://kline.sh/)
-   [Welcome to Libera Chat (HN discussion)](https://news.ycombinator.com/item?id=27207734) ([direct link to the post](https://libera.chat/news/welcome-to-libera-chat))
-   [Freenode's weak apology about the channel purging](https://freenode.net/news/post-mortem-may21)
-   [Freenode is FOSS](https://freenode.net/news/for-foss)
-   [Freenode Exodus: Projects and channels that have decided to leave Freenode](https://github.com/siraben/freenode-exodus)
-   [isfreenodedeadyet.com](https://isfreenodedeadyet.com/) - a dashboard of Freenode health?


## Closing remarks {#closing-remarks}

I was okay with a little controversy, however, actively destroying channels
that were mitigating the fear and uncertainly caused by the hostile takeover
earlier this month does not send a good message.  I'm sure everyone has a side
in this discussion, but it's hard to accept that Freenode **in any capacity**
would dream of destroying channels like #curl, #haskell, #emacs, #weechat,
\#irssi, #archlinux.  Andrew and friends are insane if they think anyone is
going to put up with this misconduct.

Given that even the existent channels are all pretty quiet, I think it's safe
to say Freenode is dead, except for niche communities that have managed to
survive this unfortunate turn of events.
