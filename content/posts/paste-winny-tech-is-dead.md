+++
title = "paste.winny.tech (Sillypaste) is dead"
author = ["Winston (winny) Weinert"]
date = 2024-05-04T00:00:00-05:00
tags = ["cloud", "infrastructure"]
draft = false
cover = "fly-io-fire.png"
+++

{{< figure src="/ox-hugo/fly-io-fire.png" >}}

What's worse than a fire on a boat?  A fire aboard an air balloon.  Rip my
[fly.io](https://fly.io/) app.

> Affected Apps:
>
> -   sillypaste-db
>
> A server hosting some of your apps has suffered irreparable hardware damage.
> Please migrate your Fly Machines to other hosts and restore volumes from any
> backups.

All good things come to an end, including this pastebin project.  If I find
myself using it again, I may spin up a fresh database if that opportunity
presents itself.

I wouldn't recommend fly.io at this time.  Having been erroneously billed (then
quickly refunded) a couple times, [fought with their V1 stack and data
corruption issues](https://community.fly.io/t/nomad-app-getting-i-o-errors-from-deployment-environment/13306/2), and it goes on.  At one time I ended up with the
PostgresSQL database container crashing with Out of Memory... [despite issuing
countless `scale` commands](https://community.fly.io/t/scaled-postgres-db-isnt-scaling/17600).  Eventually I paid for the $20/mo email support
for the support staff to tell me flatly - you must migrate and yes v1 is still
considered robust for some workloads.  I'm pretty sure the reliability claim is
a white lie, but indeed, I had to upgrade.  Turned out to be tricky due to
inconsistent documentation on V1 vs V2 features.  Had to ask [another question](https://community.fly.io/t/error-your-fly-toml-referenced-an-attribute-http-service-but-it-is-unknown-double-check-the-accepted-attributes-in-fly-toml-https-fly-io-docs-reference-configuration/10640).

It should be noted that support was quick to reply to my email because I paid
for the $20 email support plan; support on the community forums is best effort
and will not lead to a fruitful support conversation.


## What went well with Fly.io? {#what-went-well-with-fly-dot-io}

Now that fly.io's critical negative review has been written, let's review some
of Fly.io's wins.

-   [`flyctl`](https://fly.io/docs/hands-on/install-flyctl/) command line tool compares favorable against [`heroku`](https://devcenter.heroku.com/articles/heroku-cli).  Simpler
    operation, less features to confound over.
-   Its web design is modern and nice... but like many UIs designed for
    touch input, it suffers from low density... which pushed me to use the CLI
    wherever possible.
-   Support forum uses [Discourse](https://www.discourse.org/), a wonderful no-nonsense support forum experience.
-   When it works, it works really well.
-   It was a cheap experiment.  You do get what you pay for (free hosting = it
    can burn down at any time and that's ok).


## Alternatives for hosting Sillypaste {#alternatives-for-hosting-sillypaste}

I have no first-hand endorsements, but next I'd assess AWS Lambda, Digital
Ocean Apps, or a simple VPS for hosting this modest Django webapp.  A service
that gets billing right, maintains consistent documentation, and has a working
reliable PaaS offering is ideal.

For now, Sillypaste shall remain offline but the code is [always available on
GitHub](https://github.com/winny-/sillypaste).  All I need to do to remove the DNS `paste.winny.tech` entry is
delete it from my terraform project then run `terraform apply`.
