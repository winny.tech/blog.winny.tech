+++
title = "Sillypaste migrated to fly.io"
author = ["Winston (winny) Weinert"]
date = 2023-05-18T19:30:00-05:00
tags = ["devops", "computing", "python"]
draft = false
cover = "sillypaste-migration.png"
+++

{{< figure src="/ox-hugo/sillypaste-migration.png" >}}

I've been operating [Sillypaste](https://paste.winny.tech/) ([source code](https://github.com/winny-/sillypaste#readme)) - a simple [Django](https://www.djangoproject.com/) pastebin
created for [dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food).  In this post I hope to capture some of the painpoints
of working with Django, Python, [Heroku](https://heroku.com/), and the migration to [fly.io](https://fly.io/).

Ever since [Heroku sold its soul to SalesForce](https://techcrunch.com/2010/12/08/breaking-salesforce-buys-heroku-for-212-million-in-cash/) its been on [the decline](https://www.infoworld.com/article/3614210/the-decline-of-heroku.html).
Customer service is worse than ever, giving wonderful canned responses to
most questions.  It [used to be free](https://techcrunch.com/2022/08/25/heroku-announces-plans-to-eliminate-free-plans-blaming-fraud-and-abuse/) to host Sillypaste on heroku, now is a
$17/mo ordeal.  See the screenshot for a breakdown.  Notice how the "free
scheduler"[^fn:1] is not even free anymore.  They also dropped the ball when
performing the pricing structure change: [everyone and myself misunderstood if
or when our databases would be nuked if we didn't take any action](https://www.reddit.com/r/Heroku/comments/zh7ilv/what_is_wrong_with_heroku_deleting_free_dbs_for/).  I opened a
ticket about this and got another canned response from their support staff.
Thanks heroku.  The last straw that broke the camel's back was how difficult it was
to upgrade Python on Heroku.  In the end I never did figure out how to upgrade
my Django application to a newer Herok buildpack.  I had all sorts of problems,
most I have blanked out my brain because it was such a negative experience.
Something to do with EOL buildpacks and trying to upgrade a `requirements.txt`.

{{< figure src="/ox-hugo/sillypaste-heroku-costs.png" caption="<span class=\"figure-number\">Figure 1: </span>Sillypaste on Heroku costs 17.01 USD monthly" >}}

From the above criticisms I see three primary issues with Python on Heroku:

1.  Upgrading to a new [buildpack](https://devcenter.heroku.com/articles/buildpacks) is very difficult to test - you just keep
    pushing over and over again to test deploys.
2.  Python setuptools and requirements.txt is kind of annoying to work with, but
    the buildpack doesn't seem to work with some of the better package tools
    like Poetry.
3.  Heroku [is now not cost effective](https://www.heroku.com/pricing).  You can pay for a a couple budget VPSes and
    gain the same reliability, and still save money.

As the article title suggests, I decided to cut my losses and migrate from
Heroku to fly.io.  If I understand correctly, [fly.io will cost me about
0-5/month](https://fly.io/docs/about/pricing/) for the same services.  I will update this article with my findings.


## Investigation #1: Ship the django applicatinon using a Nix flake {#investigation-1-ship-the-django-applicatinon-using-a-nix-flake}

I was curious if i could ship Sillypaste as nix Flake.  It's probably possible,
but I didn't have good results.  Here's what I tried:

1.  I looked into [mach-nix](https://github.com/DavHau/mach-nix#readme), however the codebase is unmaintained, so I didn't
    want to adopt it.
2.  I looked into the mach-nix successor [dream2nix](https://github.com/nix-community/dream2nix#readme), however it choked on my
    requirements.txt entry that [pulls in package from a git repository URL](https://stackoverflow.com/questions/16584552/how-to-state-in-requirements-txt-a-direct-github-source) due
    to sandboxing.
3.  I noticed [poetry2nix](https://github.com/nix-community/poetry2nix#readme), but at this point I decided it was probably just
    another distraction from my end goal of migrating Sillypaste, given I had
    invested half a day on this research. I might investigate this later.  If I
    do, I'll edit this blog post.

Take away: nix might work, but it's still a big time sink.  Not a good
use of my time for this specific project phase.


## Investigation #2: Ship the django application using a Dockerfile {#investigation-2-ship-the-django-application-using-a-dockerfile}

One lesson I learned from the above nix foray is that many build tools work
better if you can install your Python package like any other package in your
`requirements.txt`.  To achieve this I restructured the Sillypaste codebase
into a single top level package `sillypaste` then updated various references in
the codebase.  I followed this [Django tutorial on package-izing your application](https://docs.djangoproject.com/en/4.2/intro/reusable-apps/).

After doing this, I still kept running into issues with modifying
requirements.txt (such as the case of removing Heroku support).  I called it,
`requirements.txt` bankruptcy, and migrated the project over to [Poetry](https://python-poetry.org/).  As a
nice bonus, Poetry also has a simple way to declare scripts and other
configurations in `pyproject.toml`[^fn:2]  Another nice benefit is Poetry can
set up virtualenvs for you.  I think the best feature of Poetry is its lockfile
that separates specific version data from manually-specified dependencies.

So there I was, able to run poetry install, then run `sillypaste serve` to run
the Django application in gunicorn.  Pretty cool.  For the curious [here is the
`pyproject.toml`](https://github.com/winny-/sillypaste/blob/master/pyproject.toml) (c.f. [the `poetry.lock` in the same directory](https://github.com/winny-/sillypaste/blob/master/poetry.lock)).

Next up was to write a Dockefile that installs my project as a proper python
package.  It is essentially the following - based off of [this StackOverflow Answer](https://stackoverflow.com/a/54763270/2720026):

```dockerfile
FROM python:3.10-alpine

RUN pip install poetry
RUN apk add --no-cache libpq-dev build-base

WORKDIR /src
COPY . .
RUN poetry config virtualenvs.create false \
  && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi
COPY crontab /etc/crontabs/root

WORKDIR /app
EXPOSE 8000

CMD crond && sillypaste serve
```

Note the Dockerfile also runs a cron daemon.  This is for running `sillypaste
expire` on a cadence.  This is how expired pastes and temporary user sessions are removed.

This isn't a well optimized Dockerfile, however it works and I don't think I'll
touch it again until I decide to slim it down.  I also wrote a
[docker-compose.yml](https://github.com/winny-/sillypaste/blob/master/docker-compose.yml) that does the bare minimum.


## Investigation #3: Deploying on fly.io and migrating from Heroku {#investigation-3-deploying-on-fly-dot-io-and-migrating-from-heroku}

So now I have the building blocks to deploy this application on fly.io.  I have
a Dockerfile that works well locally and seems to work on fly.io.


### Setting up the database {#setting-up-the-database}

I created the user and database as follows:

```sql
CREATE USER sillypaste WITH PASSWORD 'some top secret password';
CREATE DATABASE sillypaste WITH OWNER sillypaste;
```

Once I got the application running at `sillypaste.fly.dev`, I did a quick
database sync to test it.  Here's the command I used [from the fly.io documentation](https://fly.io/docs/rails/getting-started/migrate-from-heroku/#transfer-the-database):

```bash
pg_dump -Fc --no-acl --no-owner -d $HEROKU_DATABASE_URL |
    pg_restore --verbose --clean --no-acl --no-owner -d $DATABASE_URL
```


### And a little testing... {#and-a-little-testing-dot-dot-dot}

One weirdism I noticed was crond wouldn't fire in some cases.  This confused me
to no end.  So when I noticed that reducing the cronjob's frequency to every
minute, it seemed to fire reliably, I was like, that's good enough.  If it
costs CPU time, I'll be able to measure it and adjust if it's incurring a
service cost.  (I'm wrote this paragraph as a reminder to myself to fix this
weird behavior.)

Now the application has live data and it seems the temporary user pruning/paste
expiration runs reliably, I'm confident fly.io will work better than Heroku.


### Performing the migration {#performing-the-migration}

To do the actual migration, all I had to do was scale back my Heroku to no
dynamos, so nobody could make further database modifications via the web
frontend:

```bash
heroku ps:scale web=0
```

Next I ran the `pg_dump ... | pg_restore ...` command once more to ensure the
new database has the most recent data from the old production database.

Next I set up fly.io to use `paste.winny.tech` instead of
`sillypaste.fly.dev`.  I chose to set up A and AAAA records on
`paste.winny.tech` then issue a `fly certs create paste.winny.tech`.  I had to
issue a `fly certs check paste.winny.tech` due to some slow DNS propgatation
delays.  Otherwise it worked perfectly and I was now serving content at
<https://paste.winny.tech/> .


### Redirecting traffic from Heroku to fly.io {#redirecting-traffic-from-heroku-to-fly-dot-io}

I found [an easy-to-use Heroku "Button" named `heroku-redirect`](https://elements.heroku.com/buttons/kenmickles/heroku-redirect) (I think it's
like a pre-packaged app) to redirect all traffic to a canonical URL.

I deployed it by:

1.  Cloning the git repository somewhere
2.  Add the current git repo to the existing sillypaste heroku app: `heroku
          git:remote -a sillypaste`.  This will be used to push and also easily
    configure the application without specifying `-a sillypaste` again.
3.  Run `heroku config:add NEW_BASE_URL=http://paste.winny.tech` to configure
    the application to use this URL.
4.  Change the buildpack because yuck - heroku has all this configuration that isn't in
    your git repo: `heroku buildpacks:set heroku/nodejs`
5.  Overwrite the existing deployment and git repository: `git push -f heroku
          master`

If you reduced the scale of the workers using `heroku ps:scale`, be sure to set
the workers to more than 0:  `heroku ps:scale web=1`.


### Cleaning up - saving costs {#cleaning-up-saving-costs}

It seems heroku will happily push you to use basic dynamos instead of eco
dynamos when you change  the scale from 0 to 1.  This costs twice as much and
you probably don't want it for a hobbyist website.  Run `heroku ps:type eco` to
fix this.

Finally make sure to clean up your "free" scheduler and postgresql database, if
any.  It's probably to do it from the CLI, but I just visited the dashboard and
clicked delete a bunch.


## Other remarks {#other-remarks}

There were some weird weirdisms going on with my Django application.  Some
reason setting `DEBUG` to a boolean expression that evaluated to `False` did
not actually disable `DEBUG` mode.  [Setting it manually to False fixed it](https://github.com/winny-/sillypaste/commit/1f60e8517ea4353ee78cdc42a0d13628c1093e0f).
Bizarre (and yes there was no `DJANGO_DEBUG` envvar configured or set).

Another unrelated thing: if you find the fly.io remote builders slow, you can
also run `fly deploy --local-only` - this'll build the docker image locally
then push to your registry.

Adopting poetry was a good move.  Dumping heroku in the nearest trash
receptacle was also a good move.  I think it should be easier to operate
Sillypaste in the future since it uses a Dockerfile - no need to deal with
heroku's weird buildpacks.  I won't miss heroku.

[^fn:1]: The scheduler is still described as "free" on [their website](https://devcenter.heroku.com/articles/scheduler) and in the admin dashboard.
[^fn:2]: setuptools can also use `pyproject.toml` however I couldn't find many
    tutorials on how to do this, so I just went to Poetry.