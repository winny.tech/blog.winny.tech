+++
title = "Upgrading my Workstation to NixOS 23.11"
author = ["Winston (winny) Weinert"]
date = 2024-01-03T00:00:00-06:00
tags = ["nixos", "computing"]
draft = false
cover = "tapir.png"
+++

{{< figure src="/ox-hugo/tapir.png" caption="<span class=\"figure-number\">Figure 1: </span>Official image for 23.11" >}}

Upgrading my workstation to 23.11 wasn't as simple as I was hoping.  Ran into a
few issues.


## Performing the upgrade {#performing-the-upgrade}

I'm using a flake, so all I should have to run is:

```sh
# First edit flake.nix so it points to release-23.11.
nix flake update
nixos-rebuild boot --flake .#
```

(Then `systemctl reboot` into the new generation that includes a new kernel.)


## Issue #1: pinentry package changes {#issue-1-pinentry-package-changes}

I can't unlock any keys using gpg-agent (hence cannot use GPG keys for
accessing my password database and send signed documents).  Started when I
booted into my 23.11 generation.  Booting back into 23.05 fixed the issue, but
that's not a long term solution because 23.05 is marked as deprecated and will
be removed soon.

`journalctl --user gpg-agent.service` shows that it can't find
`/usr/bin/pinentry-gnome3`:

```text
Dec 27 14:51:40 stargate gpg-agent[4964]: can't connect to the PIN entry module '/usr/bin/pinentry-gnome3': IPC connect call failed
Dec 27 14:51:40 stargate gpg-agent[4964]: failed to unprotect the secret key: No pinentry
Dec 27 14:51:40 stargate gpg-agent[4964]: failed to read the secret key
```

It appears I have this line in `~/.gnupg/gpg-agent.conf`:

```cfg
pinentry-program pinentry-gtk2
```

When I comment this `pinentry-program` directive out, then invoke `systemctl
--user restart gpg-agent.service`, functionality is restored.  KDE's Pinentry
is used in KDE.  Not sure why, exactly.  It appears to neither use my NixOS
flake's `programs.gnupg.agent.pinentryFlavor` or I haven't rebuilt my flake
correctly.

Another weird behavior - once or twice it tried to store keys in what appeared
to be a newly-created kwallet.  It prompted me for a passphrase to protect the
typed passphrase with.  I closed the prompt and it prompted once more.  Closed
it again.  Hasn't pestered me since.


### Post-upgrade note {#post-upgrade-note}

The issue occurred again.  Verified that `pinentry-program` directive is not
present in the configuration.  Restarted `gpg-agent.service` user service.
Fixed the issue.  Confusing and not sure what happened here.


## Issue #2: docker storage was failing on upgrade {#issue-2-docker-storage-was-failing-on-upgrade}

```text
winston@stargate ~ $ sudo journalctl -u docker.service --follow
Dec 29 18:32:05 stargate dockerd[174409]: time="2023-12-29T18:32:05.514367385-06:00" level=info msg="stopping healthcheck following graceful shutdown" module=libcontainerd
Dec 29 18:32:05 stargate dockerd[174409]: time="2023-12-29T18:32:05.514408232-06:00" level=info msg="stopping event stream following graceful shutdown" error="context canceled" module=libcontainerd namespace=plugins.moby
Dec 29 18:32:06 stargate dockerd[174409]: failed to start daemon: error initializing graphdriver: prior storage driver devicemapper is deprecated and will be removed in a future release; update the the daemon configuration and explicitly choose this storage driver to continue using it; visit https://docs.docker.com/go/storage-driver/ for more information
Dec 29 18:32:06 stargate systemd[1]: docker.service: Main process exited, code=exited, status=1/FAILURE
Dec 29 18:32:06 stargate systemd[1]: docker.service: Failed with result 'exit-code'.
Dec 29 18:32:06 stargate systemd[1]: Failed to start Docker Application Container Engine.
Dec 29 18:32:08 stargate systemd[1]: docker.service: Scheduled restart job, restart counter is at 3.
Dec 29 18:32:08 stargate systemd[1]: docker.service: Start request repeated too quickly.
Dec 29 18:32:08 stargate systemd[1]: docker.service: Failed with result 'exit-code'.
Dec 29 18:32:08 stargate systemd[1]: Failed to start Docker Application Container Engine.
```

The error message `failed to start daemon: error initializing graphdriver:
prior storage driver devicemapper is deprecated and will be removed in a future
release` is particularly interesting.  [This forum post](https://discourse.nixos.org/t/docker-fails-to-start/6527) suggests docker may have
chosen the wrong storage engine on startup.  This can happen when configuration
changes or if auto-detection chooses the wrong driver.

The solution is to add the following code to your `configuration.nix`:

```nix
virtualisation.docker.storageDriver = lib.mkDefault "devicemapper";
```

I am not sure why `virtualisation.docker.storageDriver` [option](https://search.nixos.org/options?channel=23.11&show=virtualisation.docker.storageDriver&from=0&size=50&sort=relevance&type=packages&query=docker*driver) defaults to
`null` instead of a predictable default like `overlay`.


## Conclusion {#conclusion}

I'm pleased that I can now use 23.11.  I've been a NixOS user since mid-2022
have deployed it on multiple architectures, headless and graphical.  It's a
very cool place to test out new ideas.  I'm not sure if I am going to be
relying on NixOS for services that I depend on in the future, though.  Too much
churn.  I need something that isn't a surprise every time I look into doing
something with it.  Need something more predictable.  Might be Debian Testing
with nix daemon and Ansible.  Stay tuned.
