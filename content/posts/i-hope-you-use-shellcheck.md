+++
title = "I hope you use ShellCheck"
author = ["Winston (winny) Weinert"]
date = 2024-09-01T00:00:00-05:00
tags = ["linux", "computing", "shellscripting", "shellcheck"]
draft = false
cover = "bin-sh.png"
+++

In this post I hope to convince the reader on the merits of ShellCheck.  Stay
tuned for [more]({{< relref "shellcheck-with-emacs" >}}) posts about using ShellCheck.


## On Shellscripting {#on-shellscripting}

{{< figure src="/ox-hugo/bin-sh.png" >}}

[Shell scripting](https://en.wikipedia.org/wiki/Shell_script) is a of passion of mine.  Preferably Bash (here's a [guide](http://mywiki.wooledge.org/BashGuide)).
(POSIX sh a.k.a. Bourne shell works too, albeit with more effort thanks to
diminished versatility when [compared to Bash](https://www.gnu.org/software/bash/manual/html_node/Major-Differences-From-The-Bourne-Shell.html).)  The shell scripting language
family has many warts as the languages were designed for both real-time
interaction and automation programming.  Additionally, inextricable backwards
compatibility requirements are continually placed on the shell scripting
language family --- many Unix heads expect their shell to execute POSIX `sh`
scripts without a hitch.  Bourne shell, the `sh` we love and know, was
developed [in the early 70s](https://www.in-ulm.de/~mascheck/bourne/index.html#origins) and coexistent firsts in human computer interfaces.

One of my favorite shell warts is emphasis of reusing the string datatype for
all sorts of operations.  This wart is addressed, mildly, with Bash's addition
of [arrays](https://www.gnu.org/software/bash/manual/html_node/Arrays.html) and more (check out [`declare`](https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#index-declare)).  As a result, much of shell scripting
is focused on efficient text processing and the leaky abstractions that
(mostly) ["typeless" programming](https://software.codidact.com/posts/281484) provides.  For example, in order to perform
[arithmetic in `sh`](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_04), you can use a command such as `a=$(( a * 2 ))` to double
the value stored in variable `a`.

Equipped with context, it might make a little more sense as we touch on the
sheer volume of pitfalls that beget robust `sh` or Bash utilization.  Consider
the first one most script writers encounter - the infamous word split.  Refer
to the following shell snippet:

```sh
path='/my/path with spaces'
find $path -type f -name *.[ch]
```

Find will chime back with an error like:

```text
find: ‘/my/path’: No such file or directory
find: ‘with’: No such file or directory
find: ‘spaces’: No such file or directory
```


## Enter ShellCheck {#enter-shellcheck}

{{< figure src="/ox-hugo/shellcheck-diagram.png" >}}

[ShellCheck](https://www.shellcheck.net/) is a linter for `sh` family languages.  (A [linter](https://en.wikipedia.org/wiki/Lint_(software)) is a program that
scans code for common mistakes.)  I highly recommend it.  Let's see what the
value it provides for this example:

{{< figure src="/ox-hugo/shellcheck-example.png" >}}

ShellCheck catches the previous error as `Double quote to prevent globbing and
word splitting. [SC2086]`.  A brief search of `SC2086` on the internet yields
[ShellCheck: SC2086 – Double quote to prevent globbing and word splitting.](https://www.shellcheck.net/wiki/SC2086)  The
solution here is to wrap `$path` in double quotes, so the script is improved to
the following:

```sh
path='/my/path with spaces'
find "$path" -type f -name *.[ch]
```

But we're not finished yet!  Let's say the current directory contains a file
named `hello.c`, then the `-name *.[ch]` expands to `-name hello.c`.  Woops!
ShellCheck knows about this issue too: `Quote the parameter to -name so the
shell won't interpret it. [SC2061]`.  Read more here: [ShellCheck: SC2061 –
Quote the parameter to \`-name\`](https://www.shellcheck.net/wiki/SC2061).  The fix is the same, quote the parameter:

```sh
path='/my/path with spaces'
find "$path" -type f -name '*.[ch]'
```

Additional erroneous shell script examples are curated in ShellCheck's [Gallery
of bad code](https://github.com/koalaman/shellcheck/blob/master/README.md#user-content-gallery-of-bad-code).


## Ways to use ShellCheck {#ways-to-use-shellcheck}

In short, apt install it!  There are many other ways to run ShellCheck.

-   Check the official [Installing](https://github.com/koalaman/shellcheck/?tab=readme-ov-file#installing) section within the ShellCheck README.
-   Install shellcheck via your package manager (check [Repology aggregated packages](https://repology.org/project/shellcheck/versions)).
-   `pip3 install shellcheck-py` to install shellcheck via a Python Package ([GitHub repo](https://github.com/shellcheck-py/shellcheck-py))
-   Run shellcheck in an [unofficial Docker container (Debian based)](https://gitlab.com/winny/pre-commit-docker#docker) or in the [official minimal container](https://hub.docker.com/r/koalaman/shellcheck).
-   Run in CI/CD, maybe using `pre-commit`.  (I will be detailing this workflow
    in another blog post.  The Installing section within the ShellCheck README
    details one solution which I'll compare against other more user friendly
    solutions.)
-   Copy/paste into [shellcheck.net's form](https://www.shellcheck.net/) to check in browser.  Note: the checker
    runs on a server so the text you pasted is exfiltrated from your browser tab.

And last but not least, be sure to run ShellCheck in your text editor or IDE.
Good ShellCheck integration checks as you edit shell scripts, so you can catch
errors before even saving the file.


## Suppress ShellCheck {#suppress-shellcheck}

ShellCheck offers a [few different mechanisms](https://github.com/koalaman/shellcheck/wiki/Ignore) to disable specific errors via
environment variable, for the entire file, and for the next line of code.  I
usually reach for ignoring errors on a per-line basis:

1.  Insert a line before the offending line of the format `# shellcheck disable=SC2116,SC2086`
2.  ShellCheck will ignore `SC2116` and `SC2086` next time.

<!--listend-->

```bash
# shellcheck disable=SC2116,SC2086
hash=$(echo ${hash})    # trim spaces
```

(Code sample lifted from the [ShellCheck wiki's Ignore page](https://github.com/koalaman/shellcheck/wiki/Ignore).)


## That's it {#that-s-it}

There is little reason to not use ShellCheck.  Improve the quality of your work
today.  Try out ShellCheck.

Stay tuned for additional posts related to ShellCheck usage.


### P.S. If you _really_ can't use ShellCheck {#p-dot-s-dot-if-you-really-can-t-use-shellcheck}

If you can't use ShellCheck, you can still validate that scripts parse
correctly prior to execution --- check out `sh -n` and `bash -n`.  (See `set
-n` in the [POSIX sh docs](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_25_03) and in the [Bash infopages](https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html).)  The `-n` flag will parse
each line and inform you about parse errors, but not execute the parsed
commands.
