+++
title = "My 2024 in Review"
author = ["Winston (winny) Weinert"]
date = 2025-02-18T00:00:00-06:00
tags = ["lifestyle"]
draft = false
cover = "100mi-bike-ride.jpg"
+++

{{< figure src="/ox-hugo/100mi-bike-ride.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Biked &amp; camped 100 miles (160 km) from Milwaukee to Manitowoc in 36 hours" >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/x5Crps8z2Xk?si=NCWT3yGMtfJ8YOtw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It's been a tumultuous year.  It took a multitude of drafts to complete this
short piece.  I felt demoralized when authoring this post.  Then I reviewed my
2023 in review post and it put my frustration in perspective: Life is pain and
joy.  It's an undulation of anguish one day, blissful peace the next.  Keep
pushing through and learning.  That's all I should expect for myself.  The rest
is gravy.


## Things I achieved this year {#things-i-achieved-this-year}

{{< figure src="/ox-hugo/2024-paper-journals.jpg" caption="<span class=\"figure-number\">Figure 2: </span>The physical journals of 2024 (digital not included!)" >}}

1.  I sold my car and lived car free in Milwaukee.  Bikes and busses!
2.  Became an avid cyclist cruising around Milwaukee.  I also went on multi-day
    a bike tour along Eastern Wisconsin.  Stay tuned for bike content.
3.  Developed my exercise into an at-home calisthenics program (using the body
    for weight).  I was exerting reps of 60 sit ups, 15 pull ups.  While I
    cannot achieve these metrics today, soon I'll restore to my peak performance
    then exceed it.  Life had gotten in the way, and I _need_ this for myself.
4.  Began going to therapy and working on my mental well being, big time.  Wrote
    70,000 words in journal.
5.  Tutored dozens of students.  Computer literacy, Programmable Logic
    Controllers, Haskell, Python, Java, Perl, Computer Science things.  I've
    learned a lot about teaching and my students seem to enjoy working with me.
6.  Migrated most of my computers off NixOS and towards Debian Stable/Testing.
    Life is too short to dink around with self-inflicted NixOS snafus and
    community invalidating all but the savants' opinion on UX/DX.  Will NixOS
    look the same in five years, hard to say.  I do know Debian will, though!
7.  Bought a van to try vanliving in.  Sold or donated 90% of my possessions.
    The less ownership, the better I felt.  Turns out freedom (to me) means
    possessing what I _need_ and _enjoy_, not what _others say_ that I need to
    own.
8.  Moved out of Milwaukee and visiting with family and missed connections over
    the winter.  Facing one's demons is the ultimate therapy.  Van travel begins
    sometime in early 2025.
9.  Joined two book clubs and read a LOT of books for the clubs and for myself.

{{< figure src="/ox-hugo/first-van.jpg" caption="<span class=\"figure-number\">Figure 3: </span>The van for the vanlife experiment" >}}


## Goals for 2025 {#goals-for-2025}

1.  Continue with my fitness goals.  Pain free is the name of the game.  And a
    stronger core.
2.  Supplant my irrational desire for a tech job (solely for income) with a
    healthier way to earn money.  9-to-5 tech work isn't for me.  Search for
    serenity in the work.  Eyes on the flow states.  Because tech doesn't have
    flow anymore... it's mostly meetings and jumping up and down whenever boss
    asks you to.
3.  Continue with the navel gazing.  Build up the creative muscle throughout.
    Express myself in more fun ways like art and writing.
4.  Continue reading lots.  Continue writing lots.  Monetize the writing.
    Poetry is really cool too; I'll wordsmith them verses.
5.  Try vanliving and if it doesn't work out, consider moving country where car
    ownership isn't romanticized despite traffic violence manifesting as a
    fairly understood, tractable problem, yet we won't solve it here in this
    country.

{{< figure src="/ox-hugo/bikes.jpg" caption="<span class=\"figure-number\">Figure 4: </span>My main transportation for 2024.  &lt;$500 for everything in frame.  DIY saves money big time." >}}


## Thanks {#thanks}

Thanks to all my friends.  Thanks to all that listened to me hem and haw about
stuff.  Thanks to all that were willing enough to take a chance and be
vulnerable with me.  Thanks to my parents for letting me stay around for a
little while as I move on to vanliving.  Thanks to my parents dog for being
such a sweetie.

{{< figure src="/ox-hugo/scout.jpg" caption="<span class=\"figure-number\">Figure 5: </span>Scout is a good doggo" >}}

Don't put up with bullshit.  Most Americans live for about 2.44 gigaseconds.
Make each second count.
