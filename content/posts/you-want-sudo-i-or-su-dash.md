+++
title = "You want sudo -i or su -"
author = ["Winston (winny) Weinert"]
date = 2021-02-14T17:21:00-05:00
tags = ["linux", "computing", "rant"]
draft = false
+++

You want to use `sudo -i` or `su -` to log into root.  `sudo su` _anything_ is
superfluous, because you probably should be using `sudo -i` or `sudo -s`, which are
roughly equivalent, depending if you want to simulate a login (`su -` or `sudo
-i`) or not (`su` or `sudo -s`).[^fn:1]


## When to use `su -`? {#when-to-use-su}

You want to log into root using the root password.  Typically you must be in
the `wheel` group (check your PAM configuration).  In Debian, you simply need
to know the password, as [there is no `wheel` group restriction enabled by
default](https://sources.debian.org/src/util-linux/2.36.1-7/debian/util-linux.su.pam/#L8).[^fn:2]


## When to use `sudo -i`? {#when-to-use-sudo-i}

You want to log into root using your sudo configuration, which will typically
prompt for your login password or allow login without a password.  `sudo` also
logs all invocations by default.  It is also more flexible, but is also prone
to security concerns, such as the [recent local user escalation vulnerability](https://www.sudo.ws/alerts/unescape_overflow.html).
It's not crazy to consider using just `su -` or maybe some other tool like
[`doas`](https://man.openbsd.org/doas), `sudo` is a bit hard to pin down when assessing its security risks.


## Why do I want to "simulate a login"? {#why-do-i-want-to-simulate-a-login}

The next few sections show some diffs of `env(1)` output.  The diff is
generated using `diff -U0 | grep -v '^@'`.  This shows an unified diff with no
context and suppresses line markers.  First I'll summarize what the output
suggests, then show the diffs.


### TL;DR {#tl-dr}

In my case, if I don't use `sudo -i` or `su -` to simulate[^fn:3] a login the
following things might not work correctly:

-   Not exactly sure what the missing settings mean for [nix](https://nixos.org/), though I'm going to
    guess root won't be able to use nix without a login.
-   the pager settings won't be configured correctly
-   SBCL, Java, Dotnet, VBox, Fltk, OpenCL, Distcc might not work
-   Plan9port might not work
-   [`sudo` will allow `XAUTHORITY` on through](https://github.com/sudo-project/sudo/blob/7f3c670a13cb584b04ba9c9dd4d7ae457460ebda/plugins/sudoers/env.c#L213-L228), which permits you to run graphical
    programs as another user, using the current user's desktop session.  So using
    `su` may not pass this on through by default.  (Maybe `su -m` could do this as well?)

Simulating login also sets your `PWD` to root's `HOME` (`/root`).  This might
seem convenient at first.  I wonder why one would want to touch their user
files as root.  The use-cases might be (1) write a thumb drive, (2) grab some
system configuration from your user's homedir, or (3) You store system stuff in
your home directory.  Maybe logging into root's homedir is a saner default,
then just specify an absolute path to be extra clear what you wanted to do.
This also means if you do something dumb, it will not damage your user homedir,
only root's, provided you didn't `cd` somewhere else.

It most of these settings are pulled in from my `/etc/profile`.  Hence you
probably want to simulate a user login.


### The `env` diffs {#the-env-diffs}


#### `sudo -s` vs `sudo -i` {#sudo-s-vs-sudo-i}

```diff
--- sudo_-s	2021-02-14 17:26:26.912620999 -0600
+++ sudo_-i	2021-02-14 17:26:38.259214818 -0600
+PLAN9=/opt/plan9
+XDG_CONFIG_DIRS=/etc/xdg
+LESS=-R -M --shift 5
+JDK_HOME=/etc/java-config-2/current-system-vm
+CONFIG_PROTECT_MASK=/etc/sandbox.d /etc/fonts/fonts.conf /etc/gentoo-release /etc/gconf /etc/terminfo /etc/dconf /etc/ca-certificates.conf /etc/texmf/web2c /etc/texmf/language.dat.d /etc/texmf/language.def.d /etc/texmf/updmap.d /etc/revdep-rebuild
+DISTCC_VERBOSE=0
+JAVA_HOME=/etc/java-config-2/current-system-vm
+DOTNET_ROOT=/opt/dotnet_core
+ANT_HOME=/usr/share/ant
-PWD=/home/winston
+EDITOR=/usr/bin/vi
+PWD=/root
+NIX_PROFILES=/nix/var/nix/profiles/default /root/.nix-profile
+CONFIG_PROTECT=/etc/stunnel/stunnel.conf /usr/share/maven-bin-3.6/conf /usr/share/gnupg/qualified.txt /usr/share/easy-rsa /usr/share/config /usr/lib64/libreoffice/program/sofficerc
+QT_QPA_PLATFORMTHEME=qt5ct
+DISTCC_TCP_CORK=
+MANPATH=/root/.nix-profile/share/man:/etc/java-config-2/current-system-vm/man:/usr/share/gcc-data/x86_64-pc-linux-gnu/9.3.0/man:/usr/share/binutils-data/x86_64-pc-linux-gnu/2.35.1/man:/etc/java-config-2/current-system-vm/man/:/usr/local/share/man:/usr/share/man:/usr/lib/rust/man:/usr/lib/llvm/11/share/man:/opt/plan9/man
+NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs:/nix/var/nix/profiles/per-user/root/channels:/root/.nix-defexpr/channels
+OPENCL_PROFILE=nvidia
+UNCACHED_ERR_FD=
+FLTK_DOCDIR=/usr/share/doc/fltk-1.3.5-r4/html
+NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
+OPENGL_PROFILE=xorg-x11
+DISTCC_FALLBACK=1
+DCC_EMAILLOG_WHOM_TO_BLAME=
+INFOPATH=/usr/share/gcc-data/x86_64-pc-linux-gnu/9.3.0/info:/usr/share/binutils-data/x86_64-pc-linux-gnu/2.35.1/info:/usr/share/info:/usr/share/info/emacs-26
+JAVAC=/etc/java-config-2/current-system-vm/bin/javac
+LESSOPEN=|lesspipe %s
+MANPAGER=manpager
-PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/opt/bin:/usr/lib/llvm/11/bin:/opt/plan9/bin
+DISTCC_SAVE_TEMPS=0
+PAGER=/usr/bin/less
+DISTCC_SSH=
+SBCL_HOME=/usr/lib64/sbcl
+GCC_SPECS=
+GSETTINGS_BACKEND=dconf
+DISTCC_ENABLE_DISCREPANCY_EMAIL=
+XDG_DATA_DIRS=/usr/local/share:/usr/share
+PATH=/root/.nix-profile/bin:/root/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin:/usr/lib/llvm/11/bin:/opt/plan9/bin:/usr/games/bin
+VBOX_APP_HOME=/usr/lib64/virtualbox
+LV2_PATH=/usr/lib64/lv2
-_=/bin/env
+SBCL_SOURCE_ROOT=/usr/lib64/sbcl/src
+LADSPA_PATH=/usr/lib64/ladspa
+_=/usr/bin/env
```


#### `sudo su` vs `sudo su -` {#sudo-su-vs-sudo-su}

I don't have a root password set on my systems, so I will use `sudo` with `su`
for example's sake.

```diff
--- sudo_su	2021-02-14 17:25:53.932832743 -0600
+++ sudo_su_-	2021-02-14 17:26:10.259394587 -0600
+PLAN9=/opt/plan9
-SUDO_GID=1000
-SUDO_COMMAND=/bin/su
-SUDO_USER=winston
-PWD=/home/winston
+XDG_CONFIG_DIRS=/etc/xdg
+LESS=-R -M --shift 5
+JDK_HOME=/etc/java-config-2/current-system-vm
+CONFIG_PROTECT_MASK=/etc/sandbox.d /etc/fonts/fonts.conf /etc/gentoo-release /etc/gconf /etc/terminfo /etc/dconf /etc/ca-certificates.conf /etc/texmf/web2c /etc/texmf/language.dat.d /etc/texmf/language.def.d /etc/texmf/updmap.d /etc/revdep-rebuild
+DISTCC_VERBOSE=0
+JAVA_HOME=/etc/java-config-2/current-system-vm
+DOTNET_ROOT=/opt/dotnet_core
+ANT_HOME=/usr/share/ant
+EDITOR=/usr/bin/vi
+PWD=/root
+NIX_PROFILES=/nix/var/nix/profiles/default /root/.nix-profile
+CONFIG_PROTECT=/etc/stunnel/stunnel.conf /usr/share/maven-bin-3.6/conf /usr/share/gnupg/qualified.txt /usr/share/easy-rsa /usr/share/config /usr/lib64/libreoffice/program/sofficerc
-XAUTHORITY=/root/.xauthgcUQue
+QT_QPA_PLATFORMTHEME=qt5ct
+DISTCC_TCP_CORK=
+MANPATH=/root/.nix-profile/share/man:/etc/java-config-2/current-system-vm/man:/usr/share/gcc-data/x86_64-pc-linux-gnu/9.3.0/man:/usr/share/binutils-data/x86_64-pc-linux-gnu/2.35.1/man:/etc/java-config-2/current-system-vm/man/:/usr/local/share/man:/usr/share/man:/usr/lib/rust/man:/usr/lib/llvm/11/share/man:/opt/plan9/man
+NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs:/nix/var/nix/profiles/per-user/root/channels:/root/.nix-defexpr/channels
+XAUTHORITY=/root/.xauthq2CYTL
+OPENCL_PROFILE=nvidia
+UNCACHED_ERR_FD=
+FLTK_DOCDIR=/usr/share/doc/fltk-1.3.5-r4/html
+NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
+OPENGL_PROFILE=xorg-x11
+DISTCC_FALLBACK=1
+DCC_EMAILLOG_WHOM_TO_BLAME=
+INFOPATH=/usr/share/gcc-data/x86_64-pc-linux-gnu/9.3.0/info:/usr/share/binutils-data/x86_64-pc-linux-gnu/2.35.1/info:/usr/share/info:/usr/share/info/emacs-26
+JAVAC=/etc/java-config-2/current-system-vm/bin/javac
+LESSOPEN=|lesspipe %s
+MANPAGER=manpager
-PATH=/sbin:/bin:/usr/sbin:/usr/bin
-SUDO_UID=1000
-MAIL=/var/mail/root
-_=/bin/env
+DISTCC_SAVE_TEMPS=0
+PAGER=/usr/bin/less
+DISTCC_SSH=
+SBCL_HOME=/usr/lib64/sbcl
+GCC_SPECS=
+GSETTINGS_BACKEND=dconf
+DISTCC_ENABLE_DISCREPANCY_EMAIL=
+XDG_DATA_DIRS=/usr/local/share:/usr/share
+PATH=/root/.nix-profile/bin:/root/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin:/usr/lib/llvm/11/bin:/opt/plan9/bin:/usr/games/bin
+VBOX_APP_HOME=/usr/lib64/virtualbox
+LV2_PATH=/usr/lib64/lv2
+SBCL_SOURCE_ROOT=/usr/lib64/sbcl/src
+LADSPA_PATH=/usr/lib64/ladspa
+_=/usr/bin/env
```


## Conclusion {#conclusion}

Use either `sudo -i` or `su -`.  Don't mix `sudo` and `su`.  Maybe don't use
`sudo` (always good advice, though I don't follow it... yet).

[^fn:1]: While writing this post, I found that `sudo su -` will erase `SUDO_*`
    environment variables.  Maybe this is beneficial to a workflow, but in most
    cases I suggest fixing your software to not check for these vars.  [Looking at
    you, `beep`](https://github.com/winny-/beep/blob/0bd53144a2734ad9fdcb8cb136e4c8594fa9987f/beep-main.c#L480-L494).
[^fn:2]: Thanks to [tejr](https://sanctum.geek.nz/) for pointing this out.
[^fn:3]: Thanks to [tyil](https://www.tyil.nl/) for correcting my spelling.