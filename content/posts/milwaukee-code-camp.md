+++
title = "Milwaukee Code Camp"
author = ["Winston (winny) Weinert"]
date = 2019-11-23T20:24:00-05:00
tags = ["community"]
draft = false
+++

This most recent weekend (November 16th) I attended the [Milwaukee Code
Camp](https://www.milwaukeecodecamp.com/) and was pleased with the content. There was plenty of food,
coffee, and give-aways.


## The Talks {#the-talks}

I attended five talks:

1.  starting an open source project ([link](https://www.milwaukeecodecamp.com/session/details/1217))&nbsp;[^fn:1],
2.  how to manage work life balance as a software developer ([link](https://www.milwaukeecodecamp.com/session/details/1210)),
3.  getting started with [Docker](https://docs.docker.com/engine/docker-overview/) and [Kubernetes](https://kubernetes.io)[^fn:2] ([link](https://www.milwaukeecodecamp.com/session/details/1212)),
4.  introduction to [Terraform](https://www.terraform.io/) for cloud infrastructure management ([link](https://www.milwaukeecodecamp.com/Session/Details/1205)),
5.  and accessibility (a11y) on the modern web ([link](https://www.milwaukeecodecamp.com/session/details/1234)).

    I am pleased to say the open source fellow recommended GPLv3, MIT/X,
    and Apache 2.0 (and [choosealicense.com](https://choosealicense.com/)), so I have a lot of respect
    for him. I think a lesser open source evangelist would recommend one
    license, or strongly recommend one license. It really does depend on
    your project.

    I was able to get [Minikube](https://github.com/kubernetes/minikube) set up during the Docker/k8s talk in five
    minutes. No surprises when installing it from the official Gentoo
    repository. Just follow the installed readme, run the commands… it's
    really quite easy to do. A friend commented it wasn't so simple on
    their system to install Minikube and get it working.

    While I don't think I would use Terraform at this point, I have a
    good appreciation for when I might use it in the future. In
    addition, I found a [Terraform provider for libvirt](https://github.com/dmacvicar/terraform-provider-libvirt), so one could in
    theory provision their own cloud infrastructure on a simple libvirt
    cluster with Terraform. I believe this might be my first use-case
    for Terraform.

    It was refreshing to see people talking about accessibility and the
    web. I am personally not a fan of the web stack for various
    reasons.[^fn:3] But the reality is the technology won't go away, and the
    browser is the greatest common denominator both reaching users and
    simplifying the a project's platform support matrix. It was great to
    hear from somebody in industry "your website should be **somewhat**
    usable without javascript". I am really pleased to hear this; perhaps
    there is hope for the web ecosystem yet.


## The Food {#the-food}

Sweet pastries, water, soda, coffee were available in the
morning ad throughout the day. There may have been more
comprehensive breakfast items (cereal?), though I was late for the
first session. Lunch was Dominos pizza and brownies. There was no
shortage of pizza. The coffee was catered from Panera Bread.


## Conclusion {#conclusion}

In addition to learning about Terraform, Kubernetes, and
accessibility, I met a lot of cool people. I think this event was an
overwhelming success. Thanks to everybody who organized this event.

[^fn:1]: Slides [here](https://github.com/NickSchweitzer/Presentations).
[^fn:2]: See the example Docker-ized and Kubernetes-ized application on
    BitBucket [here](https://bitbucket.org/Bolbeck/mkecodecampnodemysql/src/master/).
[^fn:3]: This is a discussion in itself. For example: I think a large
    majority of the user and developer experience with web technologies
    does not adhere to the [principle of least surprise](https://en.wikipedia.org/wiki/Principle_of_least_astonishment), making it very
    frustrating for everyone involved.