+++
title = "Sway review"
author = ["Winston (winny) Weinert"]
publishDate = 2023-10-26T00:00:00-05:00
tags = ["wayland", "linux", "gentoo", "retrocomputing"]
draft = false
cover = "sway-midjourney.png"
+++

{{< figure src="/ox-hugo/sway-midjourney.png" >}}

Hello, my name is Winny and welcome to my honest review of [Sway](https://swaywm.org/), a [i3wm](https://i3wm.org/)
compatible [Wayland compositor](https://wayland.freedesktop.org/).  Its primary appeal is a compositor experience
that is easy to install, and familiar to i3 users.  For my usage, it is one of
the few compositors flexible enough to deploy on older hardware.


## Startup {#startup}

Sway does require a bit of effort and time to get it in a usable state from the
stock configuration.  That was my experience.


### Keyboard layout isn't easy to set up {#keyboard-layout-isn-t-easy-to-set-up}

When sway comes up, it'll present to you a standard US English keyboard layout.
I find this kind of strange given the main dev appears to be French.  Perhaps
it is a technical challenge, instead of design choice.  On the other hand, KDE
Wayland on NixOS picked up my keyboard layout some way or another.

Now to configure keyboard, you have to first <span class="underline">break any assumption that Sway
can pick up the keyboard layout from its environment</span>.  This isn't how it
works.  Next you'll want to review sway(1), sway(5), sway-input(5) because the
usage isn't baked into the programs so `--help` will show synopsis, but not how
to use it.  Still with me?  Great.  Next string together several discrete
concepts documented in each of the man-pages:

1.  Configuration syntax, semantics, and where the files are read from (`sway(5)`)
2.  How to change the runtime sway configuration (`swaymsg(1)`)
3.  How to make it permanent (`sway(1)`, `sway(5)`)
4.  How to select any device when using the `input` command (`input * ...`). (`sway-input(5)`)
5.  How to select the keyboard layout using the `xkb_*` settings (`sway-input(5)`):
    ```text
    input '*' xkb_layout us
    input '*' xkb_variant dvorak
    ```

This is the very first thing most non-English-native users will do, yet it requires
about 15-45 minutes of research to get past this initial hurdle.

By the way, `swaymsg input '*' xkb_variant dvorak` didn't seem to work.  I had
to restart sway to apply the above configuration changes.

Over the years I've played this dance of re-learning how to configure the
keyboard layout in Sway six times.  Thankfully this article now exists.  Some
of the funny ways that I've messed it up:

-   a few web-pages use the word "keyboard" as a placeholder like `input keyboard
        xkb_variant dvorak` and this doesn't work of course.  And you'll get no
    feedback that the device named "keyboard" is missing either.
-   Making assumptions that sway picks up keyboard from environment, partially
    caused by using chat gpt-4 (because I couldn't figure out where else to
    search, web search results are mostly line noise).
-   Not quoting the asterisk that may become a glob in certain cases
-   confusing xkb_layout with xkb_variant and getting an error message that isn't
    clear as day about what the problem is.

This list demonstrates many unique failure modes when configuring a keyboard.


### Using sway on my thinkpad x31 {#using-sway-on-my-thinkpad-x31}

{{< figure src="/ox-hugo/sway-thinkpad-x31.png" >}}

As part of my retrocomputing project, I tested sway on my old Thinkpad.  This
is important because xorg-server is not getting much love anymore.  Time to
show retrocomputer users that sway can work on their builds too!

Indeed, sway works on the thinkpad x31.  Unfortunately it does requires user
intervention to launch correctly.  `WLR_RENDERER=pixman sway` works.  Without
overriding the renderer, it'll try to access the GPU using DRI, even if you
have llvmpipe set up:

```text
radeon: Invalid PCI ID.
libEGL warning: egl: failed to create dri2 screen
00:00:00.112 [ERROR] [wlr] [EGL] command: eglInitialize, error: EGL_NOT_INITIALIZED (0x3001), message: "DRI2: failed to create screen"
radeon: Failed to get PCI ID, error number -13
libEGL warning: egl: failed to create dri2 screen
00:00:00.133 [ERROR] [wlr] [EGL] command: eglInitialize, error: EGL_NOT_INITIALIZED (0x3001), message: "DRI2: failed to create screen"
00:00:00.133 [ERROR] [wlr] [EGL] command: eglInitialize, error: EGL_NOT_INITIALIZED (0x3001), message: "eglInitialize"
00:00:00.133 [ERROR] [wlr] [render/egl.c:264] Failed to initialize EGL
00:00:00.133 [ERROR] [wlr] [render/egl.c:554] Failed to initialize EGL context
00:00:00.133 [ERROR] [wlr] [render/gles2/renderer.c:679] Could not initialize EGL
00:00:00.134 [ERROR] [wlr] [render/wlr_renderer.c:333] Could not initialize renderer
00:00:00.134 [ERROR] [sway/server.c:79] Failed to create renderer
```

Weirdly, tinywl (the wlroots example compositor) launched without user
intervention.  I can confirm llvmpipe works because under X11 it shows llvmpipe
as my driver (in glxinfo).  I asked around and [this Mesa project merge request](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/10744)
might be the long-term fix.

It's a little slow on a Pentium-M with software rendering.  Still, quite
amazing!  Alacritty, which requires 3d acceleration, runs.  glxgears gives me
20s of frames per second.  Absolutely amazing that it works!  Thanks to
llvmpipe and pixman.

Another tradeoff- utilizing CPU time for 3d acceleration yields a toasty laptop
and noisy fan, at least on this Pentium-m platform.  Irregardless, I think it
is prudent to ensure sway works on older gear - xorg-server isn't getting any
younger.


### Using sway on Gentoo {#using-sway-on-gentoo}

The gentoo ebuild ships a broken config by default.  Remove this line to fix
the default startup error:

```text
output * bg /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_1920x1080.png fill
```

It appears there is a [local `wallpapers` USE flag on sway](https://packages.gentoo.org/packages/gui-wm/sway), however it also appears the
meson configuration has a built-time flag to install wallpapers.  I think it's
possibly an upstream bug (Sway, not Gentoo) related to the template
[`config.in`](https://github.com/swaywm/sway/blob/master/config.in).


## Resources online {#resources-online}

Here's a list of resources to review if stuck with Sway;

-   i3wm documentation - it reads well, has pictures, mostly applies to sway.
-   [sway(1)](https://man.archlinux.org/man/sway.1.en), [sway(5)](https://man.archlinux.org/man/sway.5.en), [sway-input(5)](https://man.archlinux.org/man/sway-input.5.en), [sway-output(5)](https://man.archlinux.org/man/sway-output.5.en), [sway-bar(5)](https://man.archlinux.org/man/sway-bar.5.en), [sway-ipc(7)](https://man.archlinux.org/man/sway-ipc.7.en).
    [Here is a PDF for casual reading](/ox-hugo/sway-manpages.pdf).  All 68 pages; a couple more pages than
    [rsync(1)](https://linux.die.net/man/1/rsync).
-   You'll want to review wlroots, libdrm, mesa, libinput documentation.  These
    libraries are the underpinnings of sway and impact how sway works, so you'll
    have to understand how to troubleshoot these libraries too.
-   [There is a Sway wiki](https://github.com/swaywm/sway/wiki).  The wiki is easy to miss- I didn't know this website
    existed until somebody told me about it - after drafting this article.  It
    appears to discuss keyboard layouts at length.
-   Your distro's support chat.  You'll have better results asking configuration
    questions there.
-   Your wayland friend who knows way too much about wayland.


## Ratpoison style keys {#ratpoison-style-keys}

{{< figure src="/ox-hugo/ratpoison-loves-sway.png" >}}

I'm a fan of a single prefix key in window managers/terminal multiplexers.
This keeps my keyboard shortcuts free for application use.  This style of
interface control is used by [tmux](https://github.com/tmux/tmux/wiki), [screen](https://www.gnu.org/software/screen/), [ratpoison](https://www.nongnu.org/ratpoison/), [stumpwm](http://stumpwm.github.io/), and other
software. Here are few keyboard shortcuts modeling this ratpoison style of
keyboard chording.  This Thinkpad x31 machine doesn't have a windows/mod4 key,
so I was using the config with Alt/Mod1.  Using this layout I can type `C-i c`
to start a terminal emulator, and `C-i k` to close it, `C-i g 1` to go to
workspace 1, so on.

```text
bindsym Control+i mode "RootMap"
mode "RootMap" {
     bindsym Escape mode "default"
     bindsym c exec $term; mode "default"
     bindsym Exclam exec $menu; mode "default"
     bindsym k kill; mode "default"
     bindsym g mode "GroupMap"
     bindsym Backslash reload
     bindsym f fullscreen toggle; mode "default"
}

mode "GroupMap" {
     bindsym Escape mode "default"
     bindsym 1 workspace number 1; mode "default"
     bindsym 2 workspace number 2; mode "default"
     bindsym 3 workspace number 3; mode "default"
     bindsym 4 workspace number 4; mode "default"
     bindsym 5 workspace number 5; mode "default"
     bindsym 6 workspace number 6; mode "default"
     bindsym 7 workspace number 7; mode "default"
     bindsym 8 workspace number 8; mode "default"
     bindsym 9 workspace number 9; mode "default"
}
```

This set of keybinds is far from complete and unlocks basic usage of sway in my
workflows.  Some keys that I couldn't figure out:

-   `C-i C-i` should focus last focused window/container regardless of location.
-   `C-i i` should send a literal `C-i` keystroke to the current window/container.

If you know how to do this, please educate me :).


## My personal rating {#my-personal-rating}

4/5.  It does what it says on the tin - no flicker, manages windows, has a
flexible IPC for remote control, can do ratpoison style keyboard shortcuts.
Personally I find the i3wm/sway container model confusing and hard to use
correctly, so as long as I stay away from clever layouts, I may remain
productive.

The barrier to productivity is unreasonably high due to the lack of examples and
tutorial content on configuring Sway.  The hard-to-discover wiki should not be
discounted.  Hopefully this article helps reduce that barrier by a smidge.

I hope that Sway will be used in my retro-computing distro project (stay
tuned!), however <span class="underline">it may be difficult to integrate if user intervention is</span>
<span class="underline">required to repair the configuration for the user's specified language settings</span>
<span class="underline">or GPU configuration</span>.

-Winny


### Thanks {#thanks}

Major thanks to landon, bl4ckb0ne, sircmpwn, bard, and (anybody who I forgot to
list - sorry!) for reviewing this article.
