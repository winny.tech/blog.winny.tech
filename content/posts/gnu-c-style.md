+++
title = "GNU C Style"
author = ["Winston (winny) Weinert"]
date = 2019-01-13T00:00:00-06:00
tags = ["computing", "rant"]
draft = false
cover = "gnu-comic.png"
+++

No. Do not use it please! There are far easier-to-read and easier-to-use styles
for C!&nbsp;[^fn:1]

{{< figure src="/ox-hugo/gnu-comic.png" >}}

[^fn:1]: [Indentation Style on Wikipedia](https://en.wikipedia.org/wiki/Indentation_style)