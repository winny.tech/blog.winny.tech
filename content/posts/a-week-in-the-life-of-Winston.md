+++
title = "A week in the life of Winston"
author = ["Winston (winny) Weinert"]
date = 2020-04-18T19:27:00-05:00
tags = ["lifestyle", "computing", "gentoo"]
draft = false
+++

During these interesting times, I figure it would be a good idea to
describe how I've been keeping myself busy, bugs I've fixed, and some
of the daily tasks/routines that keep my day structured.

For context: I moved house on the weekend of March 21st, which is a
couple weeks before the Covid-19 fiasco became a front-and-center
concern for my geographical region. I am finishing my undergrad in
computer science — this is my last semester. The classes I am taking
are Compilers, Compiler Implementation Laboratory, and Matrices and
Applications. I am now currently living in a very rural area, so I
have been very successful in maintaining a social distance in all
aspects of my life.


## Daily routine {#daily-routine}

1.  Eat the exact same thing every day: Eggs, Bacon, Corn
    Tortillas. Take any dietary supplements/vitamins.
2.  Make coffee — Aeropressed. Currently sourcing coffee from
    [Ruby](https://rubycoffeeroasters.com/). I wish to see more Ethiopia/Kenya/Peru coffee but even a
    more earthy Colombia coffee is agreeable.
3.  Spend 30–60 minutes checking email, news, IRC.
4.  Wash up
5.  Spend about 2-3 hours on current tasks — schoolwork, bugfixing,
    packaging, or researching.
6.  Eat a lunch, make some tea
7.  Spend another 2-3 hours on same tasks.
8.  Take a break, preferably away from computer
9.  Spend another couple hours
10. Have a dinner
11. Spend some time with housemates, make sure we're all on the same page
12. Spend another couple hours on tasks
13. Wash up, goto bed

    In retrospect, I think I should replace one of those task blocks
    with off-task things such as gaming, reading books, and so
    on. That's way too much time on task, and knowing myself I end up
    being less productive due to too much time on task.


## Package work for this week {#package-work-for-this-week}

For a long time now, I've aimed to keep all my system-wide software
packaged in the OS package manager. This allows me to easily rebuild
my systems, or deploy the OS on new computers. This also means
updates become a lot easier, because the package manager can track
things such as rebuilding all library dependencies, and ensuring
dependencies are installed and are the correct versions. Depending
on your OS it's pretty easy. In my case Gentoo makes it extremely
easy.


### Alephone {#alephone}

I reintroduced [Alephone packages](https://gpo.zugaina.org/Search?search=alephone)[^fn:1] to play the classic Bungie first
person shooters Marathon, Marathon 2: Durandal, and Marathon 3:
Infinity. Thankfully I could base some of my work off my [old Portage
overlay](https://bitbucket.org/winny-/srsbuilds/src/default/games-fps/) combined with [couple-year-old commits](https://github.com/gentoo/gentoo/search?q=games-fps%252Falephone&type=Commits) from the official
Gentoo repository.

After getting a show-stopping bug addressed, I added a prerelease
package that includes fixes that should address memory corruption,
flickering sprites, and (some of the) popping audio. See details in the
Bugs Addressed section.


### The toolchain used for my university course {#the-toolchain-used-for-my-university-course}

An ongoing desire was to do all my university homeworks locally,
without logging into servers with less software choice, and an
abnormal amount of network jitter/latency spikes. I finally made it
happen with a combination of rsync invocations followed by a `tar
-czvf` and a [Gentoo package](https://gpo.zugaina.org/dev-lang/uwm-cool-bin).

I find this very exciting. I invest very heavily into my computer
environment, and try my best to avoid doing complicated work in
unfamiliar environments. I can also do work offline now.

It is worth noting that the distfiles for this package are not
publicly available, and as such you will have be a student in the
course to install it. This is intentional. I have zero interest in
trying to make this toolchain public or open source, I merely want
to use it locally.


## Bugs addressed {#bugs-addressed}


### Deal with issue making Emacs unresponsive {#deal-with-issue-making-emacs-unresponsive}

I am pleased to [discover and fix a longstanding bug](https://github.com/robert-zaremba/auto-virtualenvwrapper.el/pull/9) that would
yield my Emacs unresponsive after visiting files, then deleting the
directories the visited files resided in. I wish I had documented
the first time I noticed this problem; it may have been as soon as
I introduced `auto-virtualenvwrapper` to my workflow. This package
tells Emacs to automatically search for Python virtualenvs for use
in ansi-term (terminal in emacs), running python code from emacs,
and getting accurate tab completion when writing python.

This was one of those sort of irksome issues that is difficult to
debug unless one invests effort to reconfigure emacs to report
error traces, which can't be set after the bug occurs. As a result
every time I've encountered this bug, I have given up, and simply
restarted Emacs daemon, because messing up the minibuffer precludes
issuing a `M-x toggle-debug-on-error RET`.

I want to thank the maintainer of auto-pythonvirtualenv for being
very responsive to pull request I made. Contributing to Emacs
packages is a lot of hit-or-miss, because it seems some of the less
commonly used Emacs packages are dead. Additionally there is a
culture of disinterest in accepting PRs that don't directly improve
the maintainer's quality of life.


### Strange Alephone memory corruption {#strange-alephone-memory-corruption}

I was very excited to get Alephone packaged and installed. I
started noting weirdness on my workstation setup. It started with
some graphics corruption, with sprites being rotated 90°, and
severe visual corruption when interacting with the in-game text
terminals. Invariably on every exit the game segfaults with
[`corrupted size vs. prev_size`](https://stackoverflow.com/a/53907212/2720026).

[I reported the issue](https://github.com/Aleph-One-Marathon/alephone/issues/181), and after a lot of testing, it become
apparent the issue only occurs when playing at my native
resolution, which is 1440p (2560×1440), but does not happen at
1080p (1920×1080). Thanks to my (over) comprehensive testing and a
couple passionate project maintainers, someone was able to pinpoint
the source of the bug was due to an out of bounds write. The writes
was caused by a statically allocated buffer used to copy artifacts
of the render trees onto the screen. Or something like that.

I wrote a quick and dirty patch, then later one of the maintainers
helped write a more future-proof patch. After testing it appears
the problem is fixed. This was a fantastic experience, the
discussion was on topic, there was no bike shedding, and everybody
treated each other with kindness.


## Dropping Nvidia {#dropping-nvidia}

In 2015 I purchased a Nvidia GTX 760 used for $50. It was a great
investment. At the time AMD driver quality is pretty poor. This is
the post-fglrx horror years, but the drivers were still subpar
compared to Nvidia's proprietary drivers. You could not expect to
get Windows-par graphics performance on an AMD card in 2015. On the
other hand one could expect Windows-par graphics performance on a
Nvidia card.


### Why AMD and not Nvidia {#why-amd-and-not-nvidia}

The landscape has completely changed in the last 5 years. AMD has
open sourced their graphics drivers, and is actively helping out in
maintaining them. Nvidia on the other hand has inherit issues such
as

-   upgrading the driver breaks currently running Xorg sessions'
    3d acceleration, and requires a reboot;
-   Out of tree kernel drivers are usually a bad idea, though I
    appreciate [how easy Gentoo makes it to deal with them](https://wiki.gentoo.org/wiki/Kernel/Upgrade#Reinstalling_external_kernel_modules) — simply run
    `emerge @module-rebuild`, this still a mild annoyance because it
    adds extra steps when upgrading kernels or rebuilding kernels
-   No [native resolution modesetting is available on Nvidia](https://wiki.archlinux.org/index.php/Kernel_mode_setting), so your
    linux consoles (tt1-tty6 on most installs) are stuck at a very low
    resolution, and look very chunky;
-   ~~you have to either use Nvidia libGL or use Mesa, not both~~
    ([libglvnd](https://gitlab.freedesktop.org/glvnd/libglvnd) fixes this apparently);
-   it's yet another non-free software to install on my computer — if
    bugs occur I cannot contribute fixes, or solicit fixes from other
    users
-   OBS acts up with Nvidia binary drivers, GZdoom skyboxes are not
    captured, and certain 3d applications are somewhat difficult to
    capture correctly with the binary drivers;
-   Nvidia's composition pipeline feature for reducing video tearing
    is pretty awful. [It simply makes most animations look
    choppy/stuttery](https://www.reddit.com/r/linux_gaming/comments/5tym3e/nvidia_linux_tearing_and_forcecompositionpipeline/), and ruins the experience of most video playback;
-   Nvidia is liable to drop support for my card in another year or
    so, forcing me to upgrade anyways, [this is planned obsolescence at
    the driver level](https://www.nvidia.com/en-us/drivers/unix/legacy-gpu/). With AMD on the other hand the driver probably
    will stay in tree and supported for a couple decades;
-   There is no way to track resource usage of my GPU — it's too old
    to support tracking resource usage in `nvidia-smi`, but [`radeontop`](https://github.com/clbr/radeontop)
    has been able to do this on all AMD cards for a very long time;
-   and there [is a bug with Nvidia's HDMI alsa drivers](https://gitlab.freedesktop.org/pulseaudio/pulseaudio/issues/766) that prevents
    pulseaudio from redetecting most of my sound interfaces on s3
    resume from suspend. The usual work around is to either unplug my
    HDMI output or keep killing pulseaudio until it magically works.

    With AMD on the other hand I don't foresee most of these
    issues. Presently I found s3 suspend-resume cycles take up to a
    minute, so I need to address that. Video tearing on the other hand
    is very minimal; I have been able to watch this YouTube [tearing test](https://www.youtube.com/watch?v=MfL_JkcEFbE)
    and experience no video tearing. I did notice tearing in certain
    parts of Firewatch though. That is likely because Firewatch is not
    particularly well optimized.


### Gotchas switching cards {#gotchas-switching-cards}

The GPU arrived Thursday, and I got super excited, and neglected to
run an `emerge -uDU --changed-deps -av @word` after an `emerge
--sync`. The card installed fine, but X would segfault. I noticed
in the Xorg logs it couldn't open the `radeonsi` driver. I thought
I could simply add `amdgpu` to `VIDEO_CARDS`, but as the logs
suggest, I need `radeonsi` and `amdgpu`. [The Gentoo Wiki also
suggests this](https://wiki.gentoo.org/wiki/AMDGPU#Feature_support). Because I was both trying to update and reconfigure
my installation, this yielded to problems with blockers. It seemed
nvidia was the problem, as it was masking Xorg versions I needed. I
nuked `nvidia` from my `VIDEO_CARDS` and was successful in updating
and reconfiguring my graphics stack.

Additionally, [it appears the `vulkan` USE flag must be enabled on
`media-libs/mesa` for some Steam games to work](https://wiki.gentoo.org/wiki/Vulkan), such as The Talos
Principle. I think the Nvidia binary drivers support vulkan out of
the box, hence I never had to set a USE flag on the previous GPU
driver.

Finally, I had to configure mpv to not use vdpau (I had
forced mpv to use vdpau, for my Nvidia card). Otherwise mpv would
give me a black screen.


## Schoolwork {#schoolwork}

I found using a graphics tablet to be valuable to my math class. I
can take notes in [xournal](http://xournal.sourceforge.net/), and write problems step by step. You
might wonder what's wrong with paper, but it seems when in front of
a computer watching lectures and interacting with online learning
management systems, it is difficult to split attention between the
computer and the notebook. As such I simply decided to digitize the
notebook.

To make the experience more tolerable, I have been using
[`youtube-dl`](https://ytdl-org.github.io/youtube-dl/index.html) to grab all the videos I can, and play them locally in
[MPV](https://mpv.io/). This ensures I have global multimedia shortcuts to control
video playback, have better control over frame advance, do not
require internet access 24/7, and have better control over playback
speed.

As I finally packaged the software used for one of my classes I can
do all that class's work locally except for submission. This is
fantastic because I can use my Emacs 26 setup and do not require a
24/7 Internet connection.

My office is located in a room that can get down to the low 60°'s at
night, and I found many times I'd want to do work, I could barely
focus because I was so unevenly cold. The floor would be 60-65 but
the room would be 70. I feel like an old man complaining about this,
but really getting a space heater did wonders for my productivity
and focus. This is a schoolwork problem, because it's the most
tedious sort of productivity.


## Conclusion {#conclusion}

This has been a rather long post. I really wanted to describe some
of the things I've been up to, and some of the challenges I've been
facing. I am very happy to have removed my workstation Nvidia
dependency. I am very excited about graduating soon, and adjusting
in this time, and keeping that in mind, has been a challenge. As
usual packaging software and fixing keeps my computers usable and
maintainable.

I hope to write more in the near future. I had started some posts on
debugging a GTK bug, and some other topics, but the amount of
material to cover kept growing, much like this post keeps
growing. Stay tuned to read about seccomp madness.

[^fn:1]: See my overlay [on GitHub](https://github.com/winny-/winny-overlay).