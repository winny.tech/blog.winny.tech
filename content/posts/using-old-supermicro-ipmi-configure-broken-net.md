+++
title = "Using an old Supermicro IPMI to configure broken networking"
author = ["Winston (winny) Weinert"]
date = 2021-08-09T20:05:00-05:00
tags = ["linux", "computing"]
draft = false
cover = "winxp-ipmi.png"
+++

_The goal of this post is to demonstrate the usefulness of IPMI even in_
_hobbyist or personal use.  Anything that means less touching physical_
_machines to power cycle them, or fix network misconfigurations, can save a_
_lot of time._

I had broken my NAS's networking by adding a bridge and attaching the existing
ethernet device to it.  I forgot to configure the ethernet device to **not** try
to fetch an IP address (via DHCP), but instead only fetch an IP address on the
bridge itself.  This had the surprising effect of both the bridge (`br0`) and
ethernet (`eth0`) being given the same IP, as they both had the same mac
address (as the bridge was talking to my LAN over the `eth0` slave device).
Woops, stuff happens.

This NAS does not have HDMI, DVI, or DisplayPort.  I do have a VGA monitor, but
it was packed up in storage.  First thought was I could use a serial console,
but it appears I hadn't configured the serial console correctly, as I got a
blank terminal.  Then I remembered, this thing has IPMI with full VGA console
access.  There is a nasty catch, which is what this post is about.

For anyone curious, [here's the specs](https://linux-hardware.org/?probe=dae5ac0595) for my NAS.  It's not very exciting,
software configuration barely works, but is reliable.  A TODO is to ansible-ize
the configuration.  The motherboard, cpu were purchased used off ebay, it was a
good decision, as it was reasonably inexpensive.

Anyway back to solving that pesky networking issue...


## Using an Ubuntu VM {#using-an-ubuntu-vm}

After installing Ubuntu 20.04 in VirtualBox, I set up the guest additions
(e.g. run `sudo apt install build-essential` followed by running the
`VBoxLinuxAdditions.run` off the Guest Additions ISO.)  Then I Tried opening
the console just for fun.  I was greeted by a warning that my Java was out of
date (a Browser `alert()`), which I ignored.

{{< figure src="/ox-hugo/ubuntu-download-java.png" >}}

{{< figure src="/ox-hugo/open-launch-jnlp.png" >}}

After clicking on the console, I got a prompt to do something with the `.jnlp`
java web start file.  Ubuntu didn't know what to do with it so I gave did some
looking around and found [this helpful stock overflow post](https://askubuntu.com/questions/18792/how-can-i-view-or-start-java-web-start-applications).  It says I should
just install `icedtea-netx` (e.g. `sudo apt install icedtea-netx`).  After
installing it, then clicking on the console thumbnail again, I was again
prompted to do something with the file, but was given the option to "Open with
IcedTea Web Start (default)".  After clicking OK, I was greeted with a Java
application asking it can trust the web start application as the digital
signature could not be verified.  Here are the two warning dialogues.  I'd
recommend doing this is a throw away VM, it's old crufty software.

{{< figure src="/ox-hugo/netx-security-warning.png" >}}

{{< figure src="/ox-hugo/netx-resources.png" >}}

After clicking "Run" and "Yes" at the security prompts, I was greeted with the
VGA console.

{{< figure src="/ox-hugo/ubuntu-ipmi.png" >}}


## WinXP for the fun of it anyways? {#winxp-for-the-fun-of-it-anyways}

_I had initially set up Windows XP assuming I wouldn't be able to get this to_
_work on a modern linux distro.  But as the above section demonstrates, it's_
_possible to do it on Ubuntu 20.04.  But I figure It'd be fun to leave this_
_section in here for the reader's enjoyment.  I learned my lesson: try stuff_
_before you assume anything._

WinXP is [available on archive.org](https://archive.org/details/WinXPProSP3x86) free of change.  I don't know how the
licensing works, though it has been on archive.org for many years with CD keys,
so it must be fine, or Microsoft doesn't care.

After installing Windows XP (and installing VirtualBox guest additions), you
might need to install a browser that can access websites that use strict
TLS/SSL ciphers for HTTPS.  I found this [very cool website](https://xpforever.miraheze.org/wiki/Main_Page) that describes how
to get started with Windows XP in 2021.  Folks still run it unironically for
fun and profit.  Crazy!  But I'm also that crazy, so let's grab a good browser.

Enter [Mypal](https://www.mypal-browser.org/).  It has a cute mascot of a Raccoon.  It also seemed to work more
reliably than the version of [Artic Fox](https://github.com/wicknix/Arctic-Fox) I tried (it kept hanging ???).  You can
download it here from its GitHub [releases page](https://github.com/Feodor2/Mypal/releases).  Enjoy the following
screenshot!

{{< figure src="/ox-hugo/mypal.png" >}}

Now we have a modern-ish web browser we can access the webpages that offer Java
installers for Windows XP.  Unfortunately, Oracle seems to have removed all
references to legacy Java builds that work on Windows XP.  I get an error
message when trying the old Java releases on Windows XP:

{{< figure src="/ox-hugo/not-a-valid-win32-application.png" >}}

An hour of Google head-bashing later, I found [this link](https://www.filepuma.com/download/java_runtime_environment_32bit_7.0.5-941/) to a questionable
website called Filepuma that offers a usable Java installer.  For the esteemed
reader, here is the output of `sha256sum`:

```text
43edfa7ecde47309cb7af2ce81dd856f7299e47ae26aa387fc5db541a016bea4  Java_Runtime_Environment_(32bit)_v7.0.5.exe
```

Running it, one gets the usual security warning followed by a setup screen.
Success!

{{< figure src="/ox-hugo/java-security-warning.png" >}}

{{< figure src="/ox-hugo/java-setup.png" >}}


## Finally, accessing the console {#finally-accessing-the-console}

After a reboot, I fired up Internet Exploder again, logged into the IPMI
webpage.  After clicking on the console preview, the interactive VGA console
automatically opened up in Java WebStart.  Fanstatic!  I was able to log in to
the console and fix the networking issue.  All from my desk, without touching
the affected computer.

{{< figure src="/ox-hugo/winxp-ipmi.png" >}}
